<?php
// Heading
$_['heading_title']    = "<b>Coupon/Voucher/Reward  during checkout</b>";
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Coupon/Voucher/Reward during checkout module!';
$_['text_edit']        = 'Edit Coupon/Voucher during checkout Module';

// Entry
$_['entry_status']     	= 'Status';
$_['entry_voucher']  	= 'Voucher during Checkout';
$_['entry_coupon']  	= 'Coupon during Checkout';
$_['entry_reward']  	= 'Reward during Checkout';


// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Coupon/Voucher/Reward during checkout module!';



