<?php
class ControllerExtensionModuleFeaturedDesigners extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');
		

		$data['banners'] = array();

		/*$results = $this->model_design_banner->getBanner(9);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => HTTP_SERVER.'image/'.$result['image']
				);
			}
		}*/
$this->load->model('tool/image');
		$data['banners']=array();

		$designers=$this->db->query("SELECT dd.*,m.* FROM ".DB_PREFIX."designer_description dd LEFT JOIN ".DB_PREFIX."manufacturer m ON(dd.member_id=m.member_id)  ");

		if($designers->num_rows){

			foreach ($designers->rows as $key => $designer) {
			if($designer['member_id']){
				$data['banners'][]	=	array(
					'member_id'			=>		$designer['member_id'],
					'title'		=>		$designer['name'],
					'image'				=>		$this->model_tool_image->resize($designer['profile_image'],358,530),
					'link'				=>		$this->url->link('designer/designer','&designer_id='.$designer['member_id'],true)
				);	
				}
			}

		}
		
if(null!==$this->config->get('module_featured_designers_title')){
	$data['title']=$this->config->get('module_featured_designers_title');
}else{
	$data['title']="";
}
		if(null!==$this->config->get('module_featured_designers_description')){
		$data['description']=html_entity_decode($this->config->get('module_featured_designers_description'));
}else{
	$data['description']='';
}
		$data['module'] = $module++;

		return $this->load->view('extension/module/featured_designers', $data);
	}
}