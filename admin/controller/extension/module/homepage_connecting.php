<?php
class ControllerExtensionModuleHomepageConnecting extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/homepage_connecting');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_homepage_connecting', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/homepage_connecting', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/homepage_connecting', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['module_homepage_connecting_status'])) {
			$data['module_homepage_connecting_status'] = $this->request->post['module_homepage_connecting_status'];
		} else {
			$data['module_homepage_connecting_status'] = $this->config->get('module_homepage_connecting_status');
		}

		if (isset($this->request->post['module_homepage_connecting_title'])) {
			$data['module_homepage_connecting_title'] = $this->request->post['module_homepage_connecting_title'];
		} else {
			$data['module_homepage_connecting_title'] = $this->config->get('module_homepage_connecting_title');
		}

		if (isset($this->request->post['module_homepage_connecting_description'])) {
			$data['module_homepage_connecting_description'] = $this->request->post['module_homepage_connecting_description'];
		} else {
			$data['module_homepage_connecting_description'] = $this->config->get('module_homepage_connecting_description');
		}

		if (isset($this->request->post['module_homepage_connecting_image'])) {
			$data['module_homepage_connecting_image'] = $this->request->post['module_homepage_connecting_image'];
		} else {
			$data['module_homepage_connecting_image'] = $this->config->get('module_homepage_connecting_image');
		}

if (isset($this->request->post['module_homepage_connecting_link'])) {
			$data['module_homepage_connecting_link'] = $this->request->post['module_homepage_connecting_link'];
		} else {
			$data['module_homepage_connecting_link'] = $this->config->get('module_homepage_connecting_link');
		}

		$data['thumb']=HTTP_CATALOG.'image/'.$this->config->get('module_homepage_connecting_image');

		$data['placeholder']=HTTP_CATALOG.'image/no_image.png';

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/homepage_connecting', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/homepage_connecting')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}