<?php
// Heading
$_['heading_title']    = 'Barnoz-Fashion Module';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Barnoz-Fashion module!';
$_['text_edit']        = 'Edit Barnoz-Fashion Module';

// Entry
$_['entry_status']     = 'Status';
$_['entry_title']     = 'Name/Title of Article';
$_['entry_description']= 'Description';
$_['entry_image']= 'Image ';
$_['entry_button_link']= 'Button Link ';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Barnoz-Fashion module!';