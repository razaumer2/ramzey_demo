<?php
class ModelExtensionModuleESZFaq extends Model {

    public function getFaqs(){
    	
        $query = $this->db->query("SELECT *  FROM `" . DB_PREFIX . "eszfaq` f LEFT JOIN `" . DB_PREFIX . "eszfaq_description` fd ON (f.eszfaq_id = fd.eszfaq_id) WHERE fd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND f.status = '1' ORDER BY f.sort_order ASC");

        return $query->rows;
    }

}