<?php
class ControllerExtensionModuleCouponDuringCheckout extends Controller {
	
	public function index() {
	
	}

	public function coupon_during_checkout(){

		$data['coupon'] 	= $this->load->controller('extension/module/coupon_during_checkout/coupon');
		$data['voucher'] 	= $this->load->controller('extension/module/coupon_during_checkout/voucher');
		$data['reward'] 	= $this->load->controller('extension/module/coupon_during_checkout/reward');
		return $this->load->view('extension/module/coupon_during_checkout/index', $data);

	}

	public function coupon() {

		if ($this->config->get('total_coupon_status') && $this->config->get('module_coupon_during_checkout_status') && $this->config->get('module_coupon_during_checkout_coupon')) {

			$this->load->language('extension/total/coupon');

			if (isset($this->session->data['coupon'])) {
				$data['coupon'] = $this->session->data['coupon'];
			} else {
				$data['coupon'] = '';
			}

			return $this->load->view('extension/module/coupon_during_checkout/coupon', $data);
			
		}

	}

	public function apply_coupon() {

		$this->load->language('extension/total/coupon');

		$json = array();

		$this->load->model('extension/total/coupon');

		if (isset($this->request->post['coupon'])) {
			$coupon = $this->request->post['coupon'];
		} else {
			$coupon = '';
		}

		$coupon_info = $this->model_extension_total_coupon->getCoupon($coupon);

		if (empty($this->request->post['coupon'])) {
			$json['error'] = $this->language->get('error_empty');

			unset($this->session->data['coupon']);
		} elseif ($coupon_info) {
			$this->session->data['coupon'] = $this->request->post['coupon'];

			$json['success'] = $this->language->get('text_success');

			$json['redirect'] = $this->url->link('checkout/cart');

		} else {
			$json['error'] = $this->language->get('error_coupon');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}


	public function voucher() {

		if ($this->config->get('total_voucher_status') && $this->config->get('module_coupon_during_checkout_status') && $this->config->get('module_coupon_during_checkout_voucher')) {
			$this->load->language('extension/total/voucher');

			if (isset($this->session->data['voucher'])) {
				$data['voucher'] = $this->session->data['voucher'];
			} else {
				$data['voucher'] = '';
			}

			return $this->load->view('extension/module/coupon_during_checkout/voucher', $data);

		}
	}

	public function apply_voucher() {

		$this->load->language('extension/total/voucher');

		$json = array();

		$this->load->model('extension/total/voucher');

		if (isset($this->request->post['voucher'])) {
			$voucher = $this->request->post['voucher'];
		} else {
			$voucher = '';
		}

		$voucher_info = $this->model_extension_total_voucher->getVoucher($voucher);

		if (empty($this->request->post['voucher'])) {
			$json['error'] = $this->language->get('error_empty');
		} elseif ($voucher_info) {
			$this->session->data['voucher'] = $this->request->post['voucher'];

			$json['success'] = $this->language->get('text_success');

			$json['redirect'] = $this->url->link('checkout/cart');
		} else {
			$json['error'] = $this->language->get('error_voucher');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}


	public function reward() {

		$points = $this->customer->getRewardPoints();

		$points_total = 0;

		foreach ($this->cart->getProducts() as $product) {
			if ($product['points']) {
				$points_total += $product['points'];
			}
		}

		if ($points && $points_total && $this->config->get('total_reward_status') && $this->config->get('module_coupon_during_checkout_status') && $this->config->get('module_coupon_during_checkout_reward') ) {

			$this->load->language('extension/total/reward');

			$data['heading_title'] = sprintf($this->language->get('heading_title'), $points);

			$data['entry_reward'] = sprintf($this->language->get('entry_reward'), $points_total);

			if (isset($this->session->data['reward'])) {
				$data['reward'] = $this->session->data['reward'];
			} else {
				$data['reward'] = '';
			}
			
			return $this->load->view('extension/module/coupon_during_checkout/reward', $data);
		}

	}

	public function apply_reward() {

		$this->load->language('extension/total/reward');

		$json = array();

		$points = $this->customer->getRewardPoints();

		$points_total = 0;

		foreach ($this->cart->getProducts() as $product) {
			if ($product['points']) {
				$points_total += $product['points'];
			}
		}

		if (empty($this->request->post['reward'])) {
			$json['error'] = $this->language->get('error_reward');
		}

		if ($this->request->post['reward'] > $points) {
			$json['error'] = sprintf($this->language->get('error_points'), $this->request->post['reward']);
		}

		if ($this->request->post['reward'] > $points_total) {
			$json['error'] = sprintf($this->language->get('error_maximum'), $points_total);
		}

		if (!$json) {
			$this->session->data['reward'] = abs($this->request->post['reward']);

			$json['success'] 	= $this->language->get('text_success');

			$json['redirect'] 	= $this->url->link('checkout/cart');

			if (isset($this->request->post['redirect'])) {
				$json['redirect'] = $this->url->link($this->request->post['redirect']);
			} else {
				$json['redirect'] = $this->url->link('checkout/cart');	
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}


}