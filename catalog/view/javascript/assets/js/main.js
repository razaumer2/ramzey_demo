
/*=============================================
Table Of Contents
================================================
1. PRELOADER JS
2. JQUERY STICKY MENU
3. MENU JS
4. SLIDER JS
5. BOOTSTRAP TOOLTIP
6. FEATRED PRODUCT CAROUSELL
7. BLOG POST CAROUSELL
8. TESTIMONIAL CAROUSELL
9. PRODUCT QUICK VIEW SLIDER
10. SINGLE IMAGES SLIDER
11. BLOG GALLERY SLIDER
12. TEAM SLIDER
13. TEAM SLIDER
14. TOUCHSPIN
15. PRICE-SLIDER
16. ACCORDION JS
17. ELEVATEZOOM JS
18. BXSLIDER JS
19. SECTIONS BACKGROUNDS
20. CHAT JS
21. START MIXITUP JS
22. JQUERY LIGHTBOX
23. VISITORS POP UP


Table Of Contents end
 ================================================
 */
(function($) {
	'use strict';
	jQuery(document).on('ready', function() {


	
		/* 1. PRELOADER JS */
		$(window).on('load', function() {
			function fadeOut(el) {
				el.style.opacity = 0.4;
				var last;
				var tick = function() {
					el.style.opacity = +el.style.opacity - (new Date() - last) / 600;
					last = +new Date();
					if(+el.style.opacity > 0) {
						(window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 100);
					} else {
						el.style.display = "none";
					}
				};
				tick();
			}
			var pagePreloaderId = document.getElementById("page-preloader");
			setTimeout(function() {
				fadeOut(pagePreloaderId)
			}, 1000);
		});
		
		/* 2. JQUERY STICKY MENU */
		$(".sticky-menu").sticky({
			topSpacing: 0
		});
		$('nav#dropdown').meanmenu({
			meanMenuContainer: '.mobile-menu-area',
			meanScreenWidth: "767"
		});
		
		/* 3. MENU JS */
		$('.mainmenu-area ul.navbar-nav li a').on('click', function(e) {
			var anchor = $(this);
			$('html, body').stop().animate({
				scrollTop: $(anchor.attr('href')).offset().top - 50
			}, 1500);
			e.preventDefault();
		});
		$(window).on('scroll', function() {
			if($(this).scrollTop() > 100) {
				$('.mainmenu-area').addClass('menu-animation');
			} else {
				$('.mainmenu-area').removeClass('menu-animation');
			}
		});
		
		/* 4. SLIDER JS */
		$('.carousel').carousel({
				interval: 5000,
				pause: 'false',
			})
			
		/* 5. BOOTSTRAP TOOLTIP  */
		$('[data-toggle="tooltip"]').tooltip();
		
		/* 6. FEATRED PRODUCT CAROUSELL */
		$('.feaproduct-carousel').owlCarousel({
			autoPlay: false, //Set AutoPlay to 3 seconds
			items: 4,
			itemsDesktop: [1199, 4],
			itemsDesktopSmall: [979, 2],
			itemsTablet: [768, 1],
			pagination: false,
			navigation: true,
			navigationText: ["<i class='icofont icofont-rounded-left'></i>", "<i class='icofont icofont-rounded-right'></i>"]
		});
		
		/* 7. BLOG POST CAROUSELL */
		$('.blog-slider').owlCarousel({
			autoPlay: false, //Set AutoPlay to 3 seconds
			items: 2,
			itemsDesktop: [1199, 2],
			itemsDesktopSmall: [979, 2],
			itemsTablet: [768, 1],
			pagination: false,
			navigation: true,
			navigationText: ["<i class='icofont icofont-rounded-left'></i>", "<i class='icofont icofont-rounded-right'></i>"]
		});
		
		/* 8. TESTIMONIAL CAROUSELL */
		$('.client-testimonial').owlCarousel({
			autoPlay: false, //Set AutoPlay to 3 seconds
			items: 1,
			itemsDesktop: [1199, 1],
			itemsDesktopSmall: [979, 1],
			itemsTablet: [768, 1],
			pagination: false,
			navigation: true,
			navigationText: ["<i class='icofont icofont-rounded-left'></i>", "<i class='icofont icofont-rounded-right'></i>"]
		});
		
		/* 9. PRODUCT QUICK VIEW SLIDER */
		$('.product-quick-view-slider').owlCarousel({
			autoPlay: false, //Set AutoPlay to 3 seconds
			items: 1,
			itemsDesktop: [1199, 1],
			itemsDesktopSmall: [979, 1],
			itemsTablet: [768, 1],
			pagination: false,
			navigation: true,
			navigationText: ["<i class='icofont icofont-rounded-left'></i>", "<i class='icofont icofont-rounded-right'></i>"]
		});
		
		/* 10. SINGLE IMAGES SLIDER */
		$('.simple-images-slider').owlCarousel({
			autoPlay: false, //Set AutoPlay to 3 seconds
			items: 1,
			itemsDesktop: [1199, 1],
			itemsDesktopSmall: [979, 1],
			itemsTablet: [768, 1],
			pagination: false,
			navigation: true,
			navigationText: ["<i class='icofont icofont-rounded-left'></i>", "<i class='icofont icofont-rounded-right'></i>"]
		});
		
		/* 11. BLOG GALLERY SLIDER */
		$('.blog-gallery').owlCarousel({
			autoPlay: false, //Set AutoPlay to 3 seconds
			items: 1,
			itemsDesktop: [1199, 1],
			itemsDesktopSmall: [979, 1],
			itemsTablet: [768, 1],
			pagination: false,
			navigation: true,
			navigationText: ["<i class='icofont icofont-rounded-left'></i>", "<i class='icofont icofont-rounded-right'></i>"]
		});
		
		/* 12. TEAM SLIDER */
		$('.single-team-slider').owlCarousel({
			autoPlay: false, //Set AutoPlay to 3 seconds
			items: 3,
			itemsDesktop: [1199, 3],
			itemsDesktopSmall: [979, 2],
			itemsTablet: [768, 1],
			pagination: false,
			navigation: true,
			navigationText: ["<i class='icofont icofont-rounded-left'></i>", "<i class='icofont icofont-rounded-right'></i>"]
		});
		
		/* 13. TEAM SLIDER */
		$('.brand-slider').owlCarousel({
			autoPlay: true, //Set AutoPlay to 3 seconds
			items: 5,
			itemsDesktop: [1199, 5],
			itemsDesktopSmall: [979, 3],
			itemsTablet: [768, 2],
			pagination: false,
			navigation: false
		});
		
		/* 14. TOUCHSPIN */
		$("input[name='demo_vertical']").TouchSpin({
			verticalbuttons: true
		});
		
		/* 15. PRICE-SLIDER  */
		$("#slider-range").slider({
			range: true,
			min: 5,
			max: 1000,
			values: [200, 800],
			slide: function(event, ui) {
				$("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
			}
		});
		$("#amount").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));
		
		/* 16. ACCORDION JS  */
		var selectIds = $('#panel1,#panel2');
		$(function($) {
			selectIds.on('show.bs.collapse hidden.bs.collapse', function() {
				$(this).prev().find('.icofont').toggleClass('icofont-caret-up icofont-caret-down');
			})
		});
		var selectIds = $('#panel3,#panel4,#panel5,#panel6,#panel7,#panel8,#panel9,#panel10,#panel11');
		$(function($) {
			selectIds.on('show.bs.collapse hidden.bs.collapse', function() {
				$(this).prev().find('.icofont').toggleClass('icofont-thin-down icofont-thin-right');
			})
		});
		
		/* 17. ELEVATEZOOM JS  */
		$("#zoom1").elevateZoom({
			gallery: 'gallery_01',
			responsive: true,
			cursor: 'pointer',
			galleryActiveClass: "active",
			imageCrossfade: true
		});
		
		/* 18. BXSLIDER JS  */
		$('.bxslider').bxSlider({
			slideWidth: 83,
			slideMargin: 10,
			minSlides: 3,
			maxSlides: 4,
			pager: false,
			speed: 500,
			pause: 3000,
			adaptiveHeight: false
		});
	
	/* 19. SECTIONS BACKGROUNDS */

        var pageSection = $("div");
        pageSection.each(function(indx) {

            if ($(this).attr("data-background")) {
                $(this).css("background-image", "url(" + $(this).data("background") + ")");
            }
        });

		/* 20. CHAT JS */
		
		$.chat({
			// your user information
			userId: 1,
			// id of the room. The friends list is based on the room Id
			roomId: 1,
			// text displayed when the other user is typing
			typingText: ' is typing...',
			// text displayed when there's no other users in the room
			emptyRoomText: "There's no one around here. You can still open a session in another browser and chat with yourself :)",
			// the adapter you are using
			chatJsContentPath: '/Chatjs/',
			adapter: new DemoAdapter()
		});

	
	});
	
	/* 21. START MIXITUP JS */
	$('.product-grid').mixItUp();
	$('.blog-lists').mixItUp();
	
	/* 22. JQUERY LIGHTBOX */
	$('.venobox').venobox({
		numeratio: true,
		infinigall: true
	});
	
	
     /* 23. VISITORS POP UP */

	var my_cookie = $.cookie($('.modal-check').attr('name'));
	if (my_cookie && my_cookie == "true") {
		$(this).prop('checked', my_cookie);
		console.log('checked checkbox');
	} else {
		$('#logModal').modal('show');
		console.log('uncheck checkbox');
	}

	$(".modal-check").change(function() {
		$.cookie($(this).attr("name"), $(this).prop('checked'), {
			path: '/',
			expires: 1
		});
	});

			
			
})(jQuery);