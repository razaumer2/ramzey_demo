<?php
class ControllerExtensionModuleFootware extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/footware');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_footware', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/footware', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/footware', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['module_footware_status'])) {
			$data['module_footware_status'] = $this->request->post['module_footware_status'];
		} else {
			$data['module_footware_status'] = $this->config->get('module_footware_status');
		}

		if (isset($this->request->post['module_footware_title'])) {
			$data['module_footware_title'] = $this->request->post['module_footware_title'];
		} else {
			$data['module_footware_title'] = $this->config->get('module_footware_title');
		}

		if (isset($this->request->post['module_footware_description'])) {
			$data['module_footware_description'] = $this->request->post['module_footware_description'];
		} else {
			$data['module_footware_description'] = $this->config->get('module_footware_description');
		}

		if (isset($this->request->post['module_footware_image'])) {
			$data['module_footware_image'] = $this->request->post['module_footware_image'];
		} else {
			$data['module_footware_image'] = $this->config->get('module_footware_image');
		}

if (isset($this->request->post['module_footware_link'])) {
			$data['module_footware_link'] = $this->request->post['module_footware_link'];
		} else {
			$data['module_footware_link'] = $this->config->get('module_footware_link');
		}

		$data['thumb']=HTTP_CATALOG.'image/'.$this->config->get('module_footware_image');

		$data['placeholder']=HTTP_CATALOG.'image/no_image.png';

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/footware', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/footware')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}