<?php
class ControllerExtensionModulePopularsilks extends Controller {
	public function index() {
		$this->load->language('extension/module/popularsilks');
		$data['populars']=array();
		$popularQuery=$this->db->query("SELECT * FROM ".DB_PREFIX."popular WHERE 1");

		if($popularQuery->num_rows){
			//$data['trendings']=$popularQuery->rows;
			foreach ($popularQuery->rows as $key => $popular) {
				$data['populars'][]=array(
						'popular_id'		=> $popular['popular_id'],
						'name'				=> $popular['name'],
						'href'				=> $this->url->link('designer/fashion/info&popular_id='.$popular['popular_id'],'',true),
						'image'				=> HTTPS_SERVER.'image/'.$popular['image'],
						'thumb_pop'			=> $this->model_tool_image->resize($popular['image'],100,100),
						'description'		=> html_entity_decode($popular['description'])
				);
			}
		}

		return $this->load->view('extension/module/popularsilks', $data);
	}
}