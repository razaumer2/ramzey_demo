<?php
class ControllerExtensionModuleMatchMeWithDesigner extends Controller {
	public function index() {
		$this->load->language('extension/module/match_me_with_designer');

			$data['fields']=array();
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
					'text' => "<i class='fa fa-home'></i> Home",
					'href' => $this->url->link('common/home')
				);
		$data['breadcrumbs'][] = array(
			'text' => "Match Me With A Designer",
			'href' => $this->url->link('extension/module/match_me_with_designer')
		);
$this->document->setTitle("Match Me With A Designer");
		if ($this->request->server['REQUEST_METHOD'] == 'POST')  {

				
				if(isset($this->request->post['designer_name'])){
					$designer_name=$this->request->post['designer_name'];
				}else{
					$designer_name="";
				}
				if(isset($this->request->post['telephone'])){
					$telephone=$this->request->post['telephone'];
				}else{
					$telephone="";
				}
				if(isset($this->request->post['email'])){
					$email=$this->request->post['email'];
				}else{
					$email="";
				}
				if(isset($this->request->post['address'])){
					$address=$this->request->post['address'];
				}else{
					$address="";
				}
				if(isset($this->request->post['bust_line'])){
					$bust_line=$this->request->post['bust_line'];
				}else{
					$bust_line="";
				}
				if(isset($this->request->post['neckline_measurement'])){
					$neckline_measurement=$this->request->post['neckline_measurement'];
				}else{
					$neckline_measurement="";
				}
				if(isset($this->request->post['hip_line'])){
					$hip_line=$this->request->post['hip_line'];
				}else{
					$hip_line="";
				}
				if(isset($this->request->post['caftan_length'])){
					$caftan_length=$this->request->post['caftan_length'];
				}else{
					$caftan_length="";
				}
				if(isset($this->request->post['arm_hole'])){
					$arm_hole=$this->request->post['arm_hole'];
				}else{
					$arm_hole="";
				}
				if(isset($this->request->post['arm_circumference'])){
					$arm_circumference=$this->request->post['arm_circumference'];
				}else{
					$arm_circumference="";
				}
				if(isset($this->request->post['shoulder_line'])){
					$shoulder_line=$this->request->post['shoulder_line'];
				}else{
					$shoulder_line="";
				}
				if(isset($this->request->post['cuff_size'])){
					$cuff_size=$this->request->post['cuff_size'];
				}else{
					$cuff_size="";
				}
				if(isset($this->request->post['sleeve_length'])){
					$sleeve_length=$this->request->post['sleeve_length'];
				}else{
					$sleeve_length="";
				}
				if(isset($this->request->post['neck_deep'])){
					$neck_deep=$this->request->post['neck_deep'];
				}else{
					$neck_deep="";
				}
				if(isset($this->request->post['bottom_width'])){
					$bottom_width=$this->request->post['bottom_width'];
				}else{
					$bottom_width="";
				}
				if(isset($this->request->post['waist_line'])){
					$waist_line=$this->request->post['waist_line'];
				}else{
					$waist_line="";
				}
				if(isset($this->request->post['notes'])){
					$notes=$this->request->post['notes'];
				}else{
					$notes="";
				}
				if(isset($this->request->post['sample_clothing'])){
					$sample_clothing=$this->request->post['sample_clothing'];
				}else{
					$sample_clothing="";
				}
				if(isset($this->request->post['fabric'])){
					$fabric=$this->request->post['fabric'];
				}else{
					$fabric="";
				}

		$target_dir = DIR_IMAGE."catalog/";
		$target_file = $target_dir . basename($_FILES["myfile"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		 if (move_uploaded_file($_FILES["myfile"]["tmp_name"], $target_file)) {
		      $file=  basename( $_FILES["myfile"]["name"]);
		    }else{
		    	$file="";
		    }
				
	$this->db->query("INSERT INTO ".DB_PREFIX."custom_designs SET designer_name='".$this->db->escape($this->request->post['designer_name'])."',telephone='".$this->db->escape($this->request->post['telephone'])."',email='".$this->db->escape($this->request->post['email'])."',address='".$this->db->escape($this->request->post['address'])."',bust_line='".$this->db->escape($this->request->post['bust_line'])."',waist_line='".$this->db->escape($this->request->post['waist_line'])."',hip_line='".$this->db->escape($this->request->post['hip_line'])."',sleeve_length='".$this->db->escape($this->request->post['sleeve_length'])."',cuff_size='".$this->db->escape($this->request->post['cuff_size'])."',shoulder_line='".$this->db->escape($this->request->post['shoulder_line'])."',arm_hole='".$this->db->escape($this->request->post['arm_hole'])."',bottom_width='".$this->db->escape($this->request->post['bottom_width'])."',arm_circumference='".$this->db->escape($this->request->post['arm_circumference'])."',caftan_length='".$this->db->escape($this->request->post['caftan_length'])."',neck_deep='".$this->db->escape($this->request->post['neck_deep'])."',sample_image='".$this->db->escape($file)."',neckline_measurement='".$this->request->post['neckline_measurement']."',fabric='".$this->request->post['fabric']."',sample_clothing='".$this->request->post['sample_clothing']."',notes='".$this->db->escape($this->request->post['notes'])."',unit_neckline='".$this->request->post['unit_neckline']."',unit_bust='".$this->request->post['unit_bust']."',unit_waist='".$this->request->post['unit_waist']."',unit_sleeve='".$this->request->post['unit_sleeve']."',unit_cuff='".$this->request->post['unit_cuff']."',unit_shoulder='".$this->request->post['unit_shoulder']."',unit_hip='".$this->request->post['unit_hip']."',unit_armhole='".$this->request->post['unit_armhole']."',unit_bottom='".$this->request->post['unit_bottom']."',unit_armcircum='".$this->request->post['unit_armcircum']."',unit_caftan='".$this->request->post['unit_caftan']."',unit_neckdeep='".$this->request->post['unit_neckdeep']."'");
	$this->session->data['success'] = "Request Sent Successfully!";
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['action']=$this->url->link('extension/module/match_me_with_designer', '',true);
		$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('extension/module/match_me_with_designer', $data));
	}
}