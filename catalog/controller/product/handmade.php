<?php
class ControllerProductHandmade extends Controller {
	public function index() {
		
		$this->document->setTitle('Handmade Revival');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => 'Home',
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Handmade Revival',
			'href' => $this->url->link('product/handmade')
		);

if(null!==$this->config->get('module_handmade_status')){
	$data['module_handmade_status'] = $this->config->get('module_handmade_status');
}else{
	$data['module_handmade_status'] = 0;
}
		
$data['module_handmade_title'] = $this->config->get('module_handmade_title');
$data['module_handmade_title1'] = $this->config->get('module_handmade_title1');
$data['module_handmade_title2'] = $this->config->get('module_handmade_title2');
$data['module_handmade_title4'] = $this->config->get('module_handmade_title4');
$data['module_handmade_emb_title'] = $this->config->get('module_handmade_emb_title');
$data['module_handmade_foot_title'] = $this->config->get('module_handmade_foot_title');
$data['module_handmade_description'] = html_entity_decode($this->config->get('module_handmade_description'));
$data['module_handmade_description1'] = html_entity_decode($this->config->get('module_handmade_description1'));
$data['module_handmade_description2'] = html_entity_decode($this->config->get('module_handmade_description2'));
$data['module_handmade_description3'] = html_entity_decode($this->config->get('module_handmade_description3'));
$data['module_handmade_description4'] = html_entity_decode($this->config->get('module_handmade_description4'));
$data['module_handmade_description5'] = html_entity_decode($this->config->get('module_handmade_description5'));
$data['module_handmade_description6'] = html_entity_decode($this->config->get('module_handmade_description6'));
$data['module_handmade_description7'] = html_entity_decode($this->config->get('module_handmade_description7'));
$data['module_handmade_description8'] = html_entity_decode($this->config->get('module_handmade_description8'));
$data['module_handmade_description9'] = html_entity_decode($this->config->get('module_handmade_description9'));

	if(null!==$this->config->get('module_handmade_banner_image')){
			$data['banner_thumb']=HTTP_SERVER.'image/'.$this->config->get('module_handmade_banner_image');
		}else{
			$data['banner_thumb']=HTTP_SERVER.'image/placeholder.png';
		}

	
		if(null!==$this->config->get('module_handmade_image1')){
			$data['thumb_1']=HTTP_SERVER.'image/'.$this->config->get('module_handmade_image1');
		}else{
			$data['thumb_1']=HTTP_SERVER.'image/placeholder.png';
		}
	
		if(null!==$this->config->get('module_handmade_image2')){
			$data['thumb_2']=HTTP_SERVER.'image/'.$this->config->get('module_handmade_image2');
		}else{
			$data['thumb_2']=HTTP_SERVER.'image/placeholder.png';
		}
	
		if(null!==$this->config->get('module_handmade_image3')){
			$data['thumb_3']=HTTP_SERVER.'image/'.$this->config->get('module_handmade_image3');
		}else{
			$data['thumb_3']=HTTP_SERVER.'image/placeholder.png';
		}

		if(null!==$this->config->get('module_handmade_image4')){
			$data['thumb_4']=HTTP_SERVER.'image/'.$this->config->get('module_handmade_image4');
		}else{
			$data['thumb_4']=HTTP_SERVER.'image/placeholder.png';
		}

		if(null!==$this->config->get('module_handmade_image5')){
			$data['thumb_5']=HTTP_SERVER.'image/'.$this->config->get('module_handmade_image5');
		}else{
			$data['thumb_5']=HTTP_SERVER.'image/placeholder.png';
		}

		if(null!==$this->config->get('module_handmade_image6')){
			$data['thumb_6']=HTTP_SERVER.'image/'.$this->config->get('module_handmade_image6');
		}else{
			$data['thumb_6']=HTTP_SERVER.'image/placeholder.png';
		}

		if(null!==$this->config->get('module_handmade_image7')){
			$data['thumb_7']=HTTP_SERVER.'image/'.$this->config->get('module_handmade_image7');
		}else{
			$data['thumb_7']=HTTP_SERVER.'image/placeholder.png';
		}

		
		if(null!==$this->config->get('module_handmade_image8')){
			$data['thumb_8']=HTTP_SERVER.'image/'.$this->config->get('module_handmade_image8');
		}else{
			$data['thumb_8']=HTTP_SERVER.'image/placeholder.png';
		}

		if(null!==$this->config->get('module_handmade_image9')){
			$data['thumb_9']=HTTP_SERVER.'image/'.$this->config->get('module_handmade_image9');
		}else{
			$data['thumb_9']=HTTP_SERVER.'image/placeholder.png';
		}
	
		if(null!==$this->config->get('module_handmade_image10')){
			$data['thumb_10']=HTTP_SERVER.'image/'.$this->config->get('module_handmade_image10');
		}else{
			$data['thumb_10']=HTTP_SERVER.'image/placeholder.png';
		}
	
		if(null!==$this->config->get('module_handmade_image11')){
			$data['thumb_11']=HTTP_SERVER.'image/'.$this->config->get('module_handmade_image11');
		}else{
			$data['thumb_11']=HTTP_SERVER.'image/placeholder.png';
		}
	
		if(null!==$this->config->get('module_handmade_image12')){
			$data['thumb_12']=HTTP_SERVER.'image/'.$this->config->get('module_handmade_image12');
		}else{
			$data['thumb_12']=HTTP_SERVER.'image/placeholder.png';
		}

		

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('product/handmade', $data));
	}

}
