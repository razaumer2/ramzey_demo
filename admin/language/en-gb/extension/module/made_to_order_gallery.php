<?php
// Heading
$_['heading_title']    = 'Barnoz-Made To Order Gallery';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Made To Order Gallery module!';
$_['text_edit']        = 'Edit Made To Order Gallery Module';

// Entry
$_['entry_status']     = 'Status';
$_['entry_title']     = 'Name/Title';
$_['entry_link']     = 'Link';
$_['entry_image']     = 'Image';
$_['entry_sort_order']     = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Made To Order Gallery module!';