<?php
class ControllerExtensionModuleESZFaq extends Controller {

    public function index() {
        $this->load->language('extension/module/eszfaq');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/eszfaq')
        );

        $data['heading_title'] = $this->language->get('heading_title');

        $this->load->model('extension/module/eszfaq');

        $data['faqs'] = array();

        if($this->config->get('module_eszfaq_status')){
            $faqs = $this->model_extension_module_eszfaq->getFaqs();
        } else {
            $faqs = array();
        }

        foreach ($faqs as $faq) {
            $data['faqs'][] = array(
                'eszfaq_id' => $faq['eszfaq_id'],
                'question'  => $faq['question'],
                'answer'    => html_entity_decode($faq['answer'], ENT_QUOTES)
            );
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('extension/module/eszfaq', $data));
    }

}