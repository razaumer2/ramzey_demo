<?php
class ControllerExtensionModuleMadeToOrderGallery extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/made_to_order_gallery');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			//$this->model_setting_setting->editSetting('module_made_to_order_gallery', $this->request->post);


			$this->addMadeToOrderGalleryForm($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/made_to_order_gallery', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/made_to_order_gallery', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		$this->load->model('tool/image');
			$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

			$data['made_to_order_gallery_images']=$this->getData();

		if (isset($this->request->post['module_made_to_order_gallery_status'])) {
			$data['module_made_to_order_gallery_status'] = $this->request->post['module_made_to_order_gallery_status'];
		} else {
			$data['module_made_to_order_gallery_status'] = $this->config->get('module_made_to_order_gallery_status');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/made_to_order_gallery', $data));
	}

	public function getData(){
		$query=$this->db->query("SELECT * FROM ".DB_PREFIX."made_to_order_list WHERE 1 ");
		if($query->num_rows){
			return $query->rows;
		}else{
			return array();
		}
	}

	public function addMadeToOrderGalleryForm($data){
		if($data){
			$this->db->query("INSERT INTO ".DB_PREFIX."made_to_order SET status='".$data['module_made_to_order_gallery_status']."' ");
			$last_id=$this->db->getLastId();
			$this->db->query("DELETE FROM ".DB_PREFIX."made_to_order_list WHERE 1 ");
			foreach ($data['made_to_order_gallery_image'] as $key => $value) {

				$this->db->query("INSERT INTO ".DB_PREFIX."made_to_order_list SET made_to_order_id='".$last_id."', title='".$this->db->escape($value['title'])."', link='".$this->db->escape($value['link'])."', image='".$this->db->escape($value['image'])."', sort_order='".$this->db->escape($value['sort_order'])."' ");
			}
		}
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/made_to_order_gallery')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}