<?php
// Heading
$_['heading_title']     = 'Barnoz-Made To Order Gallerys';

// Text
$_['text_success']      = 'Success: You have modified Made To Order Gallerys!';
$_['text_list']         = 'Made To Order Gallery List';
$_['text_add']          = 'Add Made To Order Gallery';
$_['text_edit']         = 'Edit Made To Order Gallery';
$_['text_default']      = 'Default';

// Column
$_['column_name']       = 'Made To Order Gallery Name';
$_['column_status']     = 'Status';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Made To Order Gallery Name';
$_['entry_title']       = 'Title';
$_['entry_link']        = 'Link';
$_['entry_image']       = 'Image';
$_['entry_status']      = 'Status';
$_['entry_sort_order']  = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Made To Order Gallerys!';
$_['error_name']       = 'Made To Order Gallery Name must be between 3 and 64 characters!';
$_['error_title']      = 'Made To Order Gallery Title must be between 2 and 64 characters!';