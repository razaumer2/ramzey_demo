<?php
class ControllerAccountAccount extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		} 
		
		$data['edit'] = $this->url->link('account/edit', '', true);
		$data['password'] = $this->url->link('account/password', '', true);
		$data['address'] = $this->url->link('account/address', '', true);
		
		$data['credit_cards'] = array();
		
		$files = glob(DIR_APPLICATION . 'controller/extension/credit_card/*.php');
		
		foreach ($files as $file) {
			$code = basename($file, '.php');
			
			if ($this->config->get('payment_' . $code . '_status') && $this->config->get('payment_' . $code . '_card')) {
				$this->load->language('extension/credit_card/' . $code, 'extension');

				$data['credit_cards'][] = array(
					'name' => $this->language->get('extension')->get('heading_title'),
					'href' => $this->url->link('extension/credit_card/' . $code, '', true)
				);
			}
		}
		
		$data['wishlist'] = $this->url->link('account/wishlist');
		$data['order'] = $this->url->link('account/order', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		
		if ($this->config->get('total_reward_status')) {
			$data['reward'] = $this->url->link('account/reward', '', true);
		} else {
			$data['reward'] = '';
		}		
		
		$data['return'] = $this->url->link('account/return', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);
		$data['recurring'] = $this->url->link('account/recurring', '', true);
		
		$this->load->model('account/customer');
		
		$affiliate_info = $this->model_account_customer->getAffiliate($this->customer->getId());
		
		if (!$affiliate_info) {	
			$data['affiliate'] = $this->url->link('account/affiliate/add', '', true);
		} else {
			$data['affiliate'] = $this->url->link('account/affiliate/edit', '', true);
		}
		
		if ($affiliate_info) {		
			$data['tracking'] = $this->url->link('account/tracking', '', true);
		} else {
			$data['tracking'] = '';
		}
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('account/account', $data));
	}

	public function designer(){

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

			if ($this->request->server['REQUEST_METHOD'] == 'POST') {
					// var_dump($this->request->post);
					// var_dump($_FILES);
					// exit;
					if(isset($this->request->post['about_text'])){
						$about_text	=	$this->request->post['about_text'];
					}else{
						$about_text	=	"";
					}
			if(isset($this->request->post['fb_link'])){
				$fb_link=$this->request->post['fb_link'];
			}else{
				$fb_link="#";
			}
			if(isset($this->request->post['tw_link'])){
				$tw_link=$this->request->post['tw_link'];
			}else{
				$tw_link="#";
			}
			if(isset($this->request->post['insta_link'])){
				$insta_link=$this->request->post['insta_link'];
			}else{
				$insta_link="#";
			}

			$member_id=	$this->db->query("SELECT is_member FROM ".DB_PREFIX."customer WHERE customer_id='".$this->customer->getId()."' ");

			if($member_id->num_rows && $member_id->row['is_member']!=0){
				$this->db->query("DELETE FROM ".DB_PREFIX."designer_description WHERE member_id='".$member_id->row['is_member']."' ");
				$this->db->query("DELETE FROM ".DB_PREFIX."designer_collection WHERE member_id='".$member_id->row['is_member']."' ");


if(isset($this->request->post['profile_exist'])&&$this->request->post['profile_exist']!=""){
$this->db->query("INSERT INTO ".DB_PREFIX."designer_description SET about_text='".$this->db->escape($about_text)."',profile_image='".$this->db->escape($this->request->post['profile_exist'])."',fb_link='".$fb_link."',tw_link='".$tw_link."',insta_link='".$insta_link."',member_id='".$member_id->row['is_member']."' ");
}elseif(isset($_FILES["profile"])){
					$target_dir = DIR_IMAGE."catalog/";
					$target_file = $target_dir . basename($_FILES["profile"]["name"]);
					
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			// Check if image file is a actual image or fake image
			 if (move_uploaded_file($_FILES["profile"]["tmp_name"], $target_file)) {
			      $profile_image=  basename( $_FILES["profile"]["name"]);

			      $this->db->query("INSERT INTO ".DB_PREFIX."designer_description SET about_text='".$this->db->escape($about_text)."',profile_image='catalog/".$this->db->escape($profile_image)."',fb_link='".$fb_link."',tw_link='".$tw_link."',insta_link='".$insta_link."',member_id='".$member_id->row['is_member']."' ");
			    }else{
			    	$profile_image="";
			    }
				}

				

				$designer_description_id=$this->db->getLastId();


				foreach ($this->request->post['collection'] as $key => $post_value) {
if(!empty($this->request->post['collection_image_exist'])){
	foreach ($this->request->post['collection_image_exist'] as $val => $value) {
	if($key==$val){
		$this->db->query("INSERT INTO ".DB_PREFIX."designer_collection SET designer_description_id='".$designer_description_id."',member_id='".$member_id->row['is_member']."',name='".$post_value['name']."',overview='".$this->db->escape($post_value['overview'])."', image='".$value."' ");
	}
	}
	
}elseif(isset($_FILES["collection_image"])){
				$target_dir_collection = DIR_IMAGE."catalog/";
								foreach($_FILES["collection_image"]["tmp_name"] as $count=>$tmp_name) {
			if($key==$count||$_FILES["collection_image"]["tmp_name"][$count]!=""){
				//echo $_FILES["collection_image"]["name"][$count]."<br>";
			    $file_name=$_FILES["collection_image"]["name"][$count];
			    $file_tmp=$_FILES["collection_image"]["tmp_name"][$count];
			    $ext=pathinfo($file_name,PATHINFO_EXTENSION);
				$imageInfo = getimagesize( $_FILES['collection_image']['tmp_name'][$count] );
				if ( $imageInfo['mime'] == ( "image/png" ) || $imageInfo['mime'] == ( "image/jpeg" ) ||  $imageInfo['mime'] == ( "image/gif" ) || $imageInfo['mime'] == ( "image/bmp" ) ) {

			        if(!file_exists($target_dir_collection.$file_name)) {
			            move_uploaded_file($file_tmp=$_FILES["collection_image"]["tmp_name"][$count],$target_dir_collection.$file_name);
			            $this->db->query("INSERT INTO ".DB_PREFIX."designer_collection SET designer_description_id='".$designer_description_id."',member_id='".$member_id->row['is_member']."',name='".$post_value['name']."',overview='".$this->db->escape($post_value['overview'])."', image='catalog/".$file_name."' ");
			        }
			        else {
			            $filename=basename($file_name,$ext);
			            $newFileName=$filename.time().".".$ext;
			            move_uploaded_file($file_tmp=$_FILES["collection_image"]["tmp_name"][$count],$target_dir_collection.$newFileName);
			         $this->db->query("INSERT INTO ".DB_PREFIX."designer_collection SET designer_description_id='".$designer_description_id."',name='".$post_value['name']."',overview='".$this->db->escape($post_value['overview'])."',member_id='".$member_id->row['is_member']."', image='catalog/".$newFileName."' ");
			        }

			    }else{
			    	$this->response->redirect($this->url->link('account/account/designer&error=image','',true));
			    }
			    }
			}
				}
				}

			}
			$this->response->redirect($this->url->link('account/account/designer&success=yes','',true));
			}
			$data['collection_data']=array();
			$memberExist=	$this->db->query("SELECT is_member FROM ".DB_PREFIX."customer WHERE customer_id='".$this->customer->getId()."' ");
			if($memberExist->num_rows){
				$collection_data=$this->db->query("SELECT dd.*,dc.* FROM ".DB_PREFIX."designer_description dd LEFT JOIN ".DB_PREFIX."designer_collection dc ON(dd.designer_description_id=dc.designer_description_id) WHERE dd.member_id='".$memberExist->row['is_member']."' ");
					foreach ($collection_data->rows as $key => $value) {
						if($memberExist->row['is_member']==$value['member_id']){
						$data['collection_data'][]=array(
							'name'		=>	$value['name'],
							'overview'	=>	$value['overview'],
							'image'		=>	$value['image'],
							'profile_image'		=>	$value['profile_image'],
							'about_text'		=>	$value['about_text'],
							'fb_link'		=>	$value['fb_link'],
							'insta_link'		=>	$value['insta_link'],
							'tw_link'		=>	$value['tw_link']
						);
						}
					}
			

			}
			

			if (isset($this->request->get['success'])&&$this->request->get['success']=='yes') {
			$data['success'] = "Form updated successfully!";

			} else {
				$data['success'] = '';
			} 

		$this->document->setTitle("Edit Designer Form");

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => "Home",
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => "Designer Form",
			'href' => $this->url->link('account/account/designer', '', true)
		);

		$data['action']=$this->url->link("account/account/designer","",true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('account/designer', $data));

	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
