<?php
// Heading
$_['heading_title']    = 'Barnoz-Designer';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified designer module!';
$_['text_edit']        = 'Edit designer Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_banner']     = 'Banner';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify designer module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';