<?php
class ControllerExtensionModuleTestimonials extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/testimonials');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		//	$this->model_setting_setting->editSetting('module_testimonials', $this->request->post);
if(isset($this->request->get['testimonial_id'])){
			$this->db->query("UPDATE ".DB_PREFIX."testimonials SET author='".$this->db->escape($this->request->post['module_testimonials_title'])."',description='".$this->db->escape($this->request->post['module_testimonials_description'])."',date_added='".$this->db->escape($this->request->post['module_testimonials_link'])."',status='".$this->request->post['module_testimonials_status']."' WHERE testimonial_id='".$this->request->get['testimonial_id']."'");
		}else{
			$this->db->query("INSERT INTO ".DB_PREFIX."testimonials SET author='".$this->db->escape($this->request->post['module_testimonials_title'])."',description='".$this->db->escape($this->request->post['module_testimonials_description'])."',date_added='".$this->db->escape($this->request->post['module_testimonials_link'])."',status='".$this->request->post['module_testimonials_status']."'");
		}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['testimonials']=array();

		$getTestimonialsData=$this->db->query("SELECT * FROM ".DB_PREFIX."testimonials WHERE 1  ");
		if($getTestimonialsData->num_rows){
			foreach ($getTestimonialsData->rows as $key => $value) {
				$data['testimonials'][]=array(
					'testimonial_id'		=>$value['testimonial_id'],
					'author'				=>$value['author'],
					'status'				=>$value['status'],
					'date_added'				=>$value['date_added'],
					'edit'					=> $this->url->link('extension/module/testimonials&testimonial_id='.$value['testimonial_id'], 'user_token=' . $this->session->data['user_token'], true)

				);
			}
		}


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/testimonials', 'user_token=' . $this->session->data['user_token'], true)
		);
if(isset($this->request->get['testimonial_id'])){
		$data['action'] = $this->url->link('extension/module/testimonials&testimonial_id='.$this->request->get['testimonial_id'], 'user_token=' . $this->session->data['user_token'], true);
	}else{
		$data['action'] = $this->url->link('extension/module/testimonials', 'user_token=' . $this->session->data['user_token'], true);
	}

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		$data['module_testimonials_status']=0;
			$data['module_testimonials_title']="";
			$data['module_testimonials_description']="";
			$data['module_testimonials_link']="";
if(isset($this->request->get['testimonial_id'])){
	$getTestimonialsDataByID=$this->db->query("SELECT * FROM ".DB_PREFIX."testimonials WHERE testimonial_id=".$this->request->get['testimonial_id']."  ");

	if($getTestimonialsDataByID->num_rows){

			$data['module_testimonials_status']=$getTestimonialsDataByID->row['status'];
			$data['module_testimonials_title']=$getTestimonialsDataByID->row['author'];
			$data['module_testimonials_description']=html_entity_decode($getTestimonialsDataByID->row['description']);
			$data['module_testimonials_link']=$getTestimonialsDataByID->row['date_added'];

	}
}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/testimonials', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/testimonials')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}