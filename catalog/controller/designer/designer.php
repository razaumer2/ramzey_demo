<?php
class ControllerDesignerDesigner extends Controller {
	public function index() {

		$this->document->setTitle("Designers");

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => "Home",
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => "Designer",
			'href' => $this->url->link('designer/designer', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		} 
		

		
		
		
		$data['action']=$this->url->link('designer/designer', '', true);



		
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		if(!isset($this->request->get['designer_id'])){



			$designer_list=$this->db->query("SELECT mr.*,mb.* FROM ".DB_PREFIX."manufacturer mr LEFT JOIN ".DB_PREFIX."members mb ON(mr.member_id=mb.member_id) WHERE 1 ");
			$data['designer_list']=array();

			$data['designer_list']=$designer_list->rows;
		/*	echo "<pre>";
var_dump($data['designer_list']);
echo "</pre>";
exit;*/
				$this->response->setOutput($this->load->view('designer/designer_list', $data));
		}else{


			$this->load->model('tool/image');

		$designer=$this->db->query("SELECT mr.*,mb.* FROM ".DB_PREFIX."manufacturer mr LEFT JOIN ".DB_PREFIX."members mb ON(mr.member_id=mb.member_id) WHERE mb.member_id='".$this->request->get['designer_id']."' ");
		$data['designer_name']="";
		if($designer->num_rows){
			$data['designer_name']=$designer->row['designer_name']." | ".$designer->row['brand_name'];

			$data['email']=$designer->row['email'];
			$data['manufacturer_id']=$designer->row['manufacturer_id'];
			$data['manufacturer_products']=$this->getManufacturerProducts($designer->row['manufacturer_id']);
			$data['phone']=$designer->row['phone'];
			if(isset($designer->row['brand_logo'])){

			$data['brand_logo']= $this->model_tool_image->resize('catalog/'.$designer->row['brand_logo'],230,170);
		}else{
			$data['brand_logo']= $this->model_tool_image->resize('no-image.pmg',230,70);
		}
			$data['email']=$designer->row['email'];
		}

$this->document->setTitle($data['designer_name']." - Designer");
		$data['collection']=array();
		$data['collection_row']=array();

		$collection_query	=	$this->db->query("SELECT * FROM ".DB_PREFIX."designer_description WHERE member_id='".$this->request->get['designer_id']."' ");

		if($collection_query->num_rows){
			$data['collection_row']=$collection_query->row;
$collection=$this->db->query("SELECT  *  FROM ".DB_PREFIX."designer_collection  WHERE designer_description_id='".$collection_query->row['designer_description_id']."' and member_id='".$this->request->get['designer_id']."'  ");
			if($collection->num_rows){
			$data['collection']=$collection->rows;
			}
		}



		$data['designer_works']=array();

		$designer_works=$this->db->query("SELECT *  FROM ".DB_PREFIX."designer_images WHERE member_id='".$this->request->get['designer_id']."'  ");
		if($designer_works->num_rows){
			$data['designer_works']=$designer_works->rows;
		}

				$this->response->setOutput($this->load->view('designer/designers', $data));
		}
		
	
	}

	public function getManufacturerProducts($manufacturer_id){
$data['products']=array();
		$this->load->language('product/manufacturer');

		$this->load->model('catalog/manufacturer');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);

		if ($manufacturer_info) {
			

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			

			$data['products'] = array();

			$filter_data = array(
				'filter_manufacturer_id' => $manufacturer_id
				
			);

			$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

			$results = $this->model_catalog_product->getProducts($filter_data);

			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height'));
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'location'		=> $result['location'],
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],
					'href'        => $this->url->link('product/product', 'manufacturer_id=' . $result['manufacturer_id'] . '&product_id=' . $result['product_id'] . $url)
				);
			}
			return $data['products'];
	}
}

	public function info(){


		$this->document->setTitle("Collection Overview");

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => "Home",
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => "Designer",
			'href' => $this->url->link('designer/designer', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		} 
		$data['collection_overview']=array();
		if(isset($this->request->get['designer_collection_id'])){
			$query=$this->db->query("SELECT * FROM ".DB_PREFIX."designer_collection WHERE designer_collection_id='".$this->request->get['designer_collection_id']."' ");
			if($query->num_rows){
				$data['collection_overview']=$query->row;
			}
		}
		
		$data['action']=$this->url->link('designer/designer', '', true);	
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

				$this->response->setOutput($this->load->view('designer/designer_info', $data));

	}

	
}
