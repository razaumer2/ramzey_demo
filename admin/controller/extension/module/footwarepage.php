<?php
class ControllerExtensionModuleFootwarepage extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/footwarepage');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_footwarepage', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module/footwarepage', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/footwarepage', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/footwarepage', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['module_footwarepage_status'])) {
			$data['module_footwarepage_status'] = $this->request->post['module_footwarepage_status'];
		} else {
			$data['module_footwarepage_status'] = $this->config->get('module_footwarepage_status');
		}

		if (isset($this->request->post['module_footwarepage_title'])) {
			$data['module_footwarepage_title'] = $this->request->post['module_footwarepage_title'];
		} else {
			$data['module_footwarepage_title'] = $this->config->get('module_footwarepage_title');
		}

		if (isset($this->request->post['module_footwarepage_description'])) {
			$data['module_footwarepage_description'] = $this->request->post['module_footwarepage_description'];
		} else {
			$data['module_footwarepage_description'] = $this->config->get('module_footwarepage_description');
		}

		if (isset($this->request->post['module_footwarepage_image'])) {
			$data['module_footwarepage_image'] = $this->request->post['module_footwarepage_image'];
		} else {
			$data['module_footwarepage_image'] = $this->config->get('module_footwarepage_image');
		}



if (isset($this->request->post['module_footwarepage_title_1'])) {
			$data['module_footwarepage_title_1'] = $this->request->post['module_footwarepage_title_1'];
		} else {
			$data['module_footwarepage_title_1'] = $this->config->get('module_footwarepage_title_1');
		}

		if (isset($this->request->post['module_footwarepage_description_1'])) {
			$data['module_footwarepage_description_1'] = $this->request->post['module_footwarepage_description_1'];
		} else {
			$data['module_footwarepage_description_1'] = $this->config->get('module_footwarepage_description_1');
		}

		if (isset($this->request->post['module_footwarepage_image_1'])) {
			$data['module_footwarepage_image_1'] = $this->request->post['module_footwarepage_image_1'];
		} else {
			$data['module_footwarepage_image_1'] = $this->config->get('module_footwarepage_image_1');
		}

if(null!==$this->config->get('module_footwarepage_image_1')){
	$data['thumb_1']=HTTP_CATALOG.'image/'.$this->config->get('module_footwarepage_image_1');

}else{
	$data['thumb_1']=HTTP_CATALOG.'image/no_image.png';
}

$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->request->post['module_footwarepage_product_name'])) {
			$data['module_footwarepage_product_name'] = $this->request->post['module_footwarepage_product_name'];
		} 
		 else {
			$data['module_footwarepage_product_name'] = $this->config->get('module_footwarepage_product_name');
		}

$this->load->model('catalog/product');

		$data['products'] = array();

		if (!empty($this->request->post['module_footwarepage_products'])) {
			$products = $this->request->post['module_footwarepage_products'];
		} elseif (null!==$this->config->get('module_footwarepage_products')) {
			$products = $this->config->get('module_footwarepage_products');
		} else {
			$products = array();
		}

		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);

			if ($product_info) {
				$data['products'][] = array(
					'product_id' => $product_info['product_id'],
					'name'       => $product_info['name']
				);
			}
		}


if (isset($this->request->post['module_footwarepage_title_2'])) {
			$data['module_footwarepage_title_2'] = $this->request->post['module_footwarepage_title_2'];
		} else {
			$data['module_footwarepage_title_2'] = $this->config->get('module_footwarepage_title_2');
		}

		if (isset($this->request->post['module_footwarepage_description_2'])) {
			$data['module_footwarepage_description_2'] = $this->request->post['module_footwarepage_description_2'];
		} else {
			$data['module_footwarepage_description_2'] = $this->config->get('module_footwarepage_description_2');
		}

		if (isset($this->request->post['module_footwarepage_image_2'])) {
			$data['module_footwarepage_image_2'] = $this->request->post['module_footwarepage_image_2'];
		} else {
			$data['module_footwarepage_image_2'] = $this->config->get('module_footwarepage_image_2');
		}

if(null!==$this->config->get('module_footwarepage_image_2')){
	$data['thumb_2']=HTTP_CATALOG.'image/'.$this->config->get('module_footwarepage_image_2');

}else{
	$data['thumb_2']=HTTP_CATALOG.'image/no_image.png';
}

if (isset($this->request->post['module_footwarepage_link'])) {
			$data['module_footwarepage_link'] = $this->request->post['module_footwarepage_link'];
		} else {
			$data['module_footwarepage_link'] = $this->config->get('module_footwarepage_link');
		}
if(null!==$this->config->get('module_footwarepage_image')){
	$data['thumb']=HTTP_CATALOG.'image/'.$this->config->get('module_footwarepage_image');

}else{
	$data['thumb']=HTTP_CATALOG.'image/no_image.png';
}


if (isset($this->request->post['module_footwarepage_title'])) {
			$data['module_footwarepage_title'] = $this->request->post['module_footwarepage_title'];
		} else {
			$data['module_footwarepage_title'] = $this->config->get('module_footwarepage_title');
		}

		if (isset($this->request->post['module_footwarepage_description'])) {
			$data['module_footwarepage_description'] = $this->request->post['module_footwarepage_description'];
		} else {
			$data['module_footwarepage_description'] = $this->config->get('module_footwarepage_description');
		}

		if (isset($this->request->post['module_footwarepage_image'])) {
			$data['module_footwarepage_image'] = $this->request->post['module_footwarepage_image'];
		} else {
			$data['module_footwarepage_image'] = $this->config->get('module_footwarepage_image');
		}

if(null!==$this->config->get('module_footwarepage_image')){
	$data['thumb']=HTTP_CATALOG.'image/'.$this->config->get('module_footwarepage_image');

}else{
	$data['thumb']=HTTP_CATALOG.'image/no_image.png';
}


if (isset($this->request->post['module_footwarepage_title'])) {
			$data['module_footwarepage_title'] = $this->request->post['module_footwarepage_title'];
		} else {
			$data['module_footwarepage_title'] = $this->config->get('module_footwarepage_title');
		}

		if (isset($this->request->post['module_footwarepage_description'])) {
			$data['module_footwarepage_description'] = $this->request->post['module_footwarepage_description'];
		} else {
			$data['module_footwarepage_description'] = $this->config->get('module_footwarepage_description');
		}

		if (isset($this->request->post['module_footwarepage_image'])) {
			$data['module_footwarepage_image'] = $this->request->post['module_footwarepage_image'];
		} else {
			$data['module_footwarepage_image'] = $this->config->get('module_footwarepage_image');
		}

if(null!==$this->config->get('module_footwarepage_image')){
	$data['thumb']=HTTP_CATALOG.'image/'.$this->config->get('module_footwarepage_image');

}else{
	$data['thumb']=HTTP_CATALOG.'image/no_image.png';
}

		//$data['thumb']=HTTP_CATALOG.'image/'.$this->config->get('module_footwarepage_image');

		$data['placeholder']=HTTP_CATALOG.'image/no_image.png';

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/footwarepage', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/footwarepage')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}