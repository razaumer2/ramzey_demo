<?php
class ControllerDesignerFashion extends Controller {
	public function index() {

		$this->document->setTitle("Fashion");

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => "Home",
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => "Fashion",
			'href' => $this->url->link('designer/fashion', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		} 
		
		$data['action']=$this->url->link('designer/fashion', '', true);


		if (null!==$this->config->get('module_fashion_modest_title')) {
			$data['module_fashion_modest_title'] = $this->config->get('module_fashion_modest_title');
		} else {
			$data['module_fashion_modest_title'] = "";
		}

		if (null!==$this->config->get('module_fashion_wedding_title')) {
			$data['module_fashion_wedding_title'] = $this->config->get('module_fashion_wedding_title');
		} else {
			$data['module_fashion_wedding_title'] = "";
		}

		if (null!==$this->config->get('module_fashion_title_2')) {
			$data['module_fashion_title_2'] =  $this->config->get('module_fashion_title_2');
		} else {
			$data['module_fashion_title_2'] = "";
		}
		

		if (null!==$this->config->get('module_wedding_description')) {
			$data['module_wedding_description'] = html_entity_decode($this->config->get('module_wedding_description'), ENT_QUOTES);
		} else {
			$data['module_wedding_description'] = "";
		}

		

		if (null!==$this->config->get('module_fashion_description')) {
			$data['module_fashion_description'] = html_entity_decode($this->config->get('module_fashion_description'), ENT_QUOTES);
		} else {
			$data['module_fashion_description'] = "";
		}

		if (null!==$this->config->get('module_fashion_description_1')) {
			$data['module_fashion_description_1'] =  html_entity_decode($this->config->get('module_fashion_description_1'), ENT_QUOTES);
		} else {
			$data['module_fashion_description_1'] ="";
		}

		if (null!==$this->config->get('module_fashion_description_2')) {
			$data['module_fashion_description_2'] = html_entity_decode($this->config->get('module_fashion_description_2'), ENT_QUOTES);
		} else {
			$data['module_fashion_description_2'] = "";
		}

		if (null!==$this->config->get('module_fashion_description_3')) {
			$data['module_fashion_description_3'] =html_entity_decode($this->config->get('module_fashion_description_3'), ENT_QUOTES);
		} else {
			$data['module_fashion_description_3'] = "";
		}

		if (null!==$this->config->get('module_fashion_description_4')) {
			$data['module_fashion_description_4'] = html_entity_decode($this->config->get('module_fashion_description_4'), ENT_QUOTES);
		} else {
			$data['module_fashion_description_4'] = "";
		}

if (null!==$this->config->get('module_fashion_wedding_description')) {
			$data['module_fashion_wedding_description'] = html_entity_decode($this->config->get('module_fashion_wedding_description'), ENT_QUOTES);
		} else {
			$data['module_fashion_wedding_description'] = "";
		}

		

		if (null!==$this->config->get('module_fashion_image')) {
			$data['module_fashion_image'] = $this->config->get('module_fashion_image');
		} else {
			$data['module_fashion_image'] = "";
		}

		if (null!==$this->config->get('module_fashion_wdding_image')) {
			$data['module_fashion_wdding_image'] = $this->config->get('module_fashion_wdding_image');
		} else {
			$data['module_fashion_wdding_image'] = "";
		}



		if(null!==$this->config->get('module_fashion_wdding_image')){
			$data['thumb_wed']=HTTPS_SERVER.'image/'.$this->config->get('module_fashion_wdding_image');
		}else{
			$data['thumb_wed']=HTTPS_SERVER.'image/no_image.png';
		}


		if(null!==$this->config->get('module_fashion_image')){
			$data['thumb']=HTTPS_SERVER.'image/'.$this->config->get('module_fashion_image');
		}else{
			$data['thumb']=HTTPS_SERVER.'image/no_image.png';
		}

		$data['trendings']=array();
		$data['populars']=array();
		$data['modests']=array();
		$this->load->model('tool/image');
		$trendingQuery=$this->db->query("SELECT * FROM ".DB_PREFIX."trending WHERE 1");

		if($trendingQuery->num_rows){
			foreach ($trendingQuery->rows as $key => $trending) {
				$data['trendings'][]=array(
						'trending_id'		=> $trending['trending_id'],
						'name'				=> $trending['name'],
						'href'				=> $this->url->link('designer/fashion/info&trending_id='.$trending['trending_id'],'',true),
						'image'				=> HTTPS_SERVER.'image/'.$trending['image'],
						'thumb_trend'		=> $this->model_tool_image->resize($trending['image'],100,100),
						'description'		=> html_entity_decode($trending['description'])
				);
			}
			
		}

		$popularQuery=$this->db->query("SELECT * FROM ".DB_PREFIX."popular WHERE 1");

		if($popularQuery->num_rows){
			//$data['trendings']=$popularQuery->rows;
			foreach ($popularQuery->rows as $key => $popular) {
				$data['populars'][]=array(
						'popular_id'		=> $popular['popular_id'],
						'name'				=> $popular['name'],
						'href'				=> $this->url->link('designer/fashion/info&popular_id='.$popular['popular_id'],'',true),
						'image'				=> HTTPS_SERVER.'image/'.$popular['image'],
						'thumb_pop'			=> $this->model_tool_image->resize($popular['image'],100,100),
						'description'		=> html_entity_decode($popular['description'])
				);
			}
		}

		$modestQuery=$this->db->query("SELECT * FROM ".DB_PREFIX."modest WHERE 1");

		if($modestQuery->num_rows){
			//$data['trendings']=$modestQuery->rows;
			foreach ($modestQuery->rows as $key => $modest) {
				$data['modests'][]=array(
						'modest_id'			=> $modest['modest_id'],
						'name'				=> $modest['name'],
						'href'				=> $this->url->link('designer/fashion/info&modest_id='.$modest['modest_id'],'',true),
						'image'				=> HTTPS_SERVER.'image/'.$modest['image'],
						'thumb_mod'			=> $this->model_tool_image->resize($modest['image'],100,100),
						'description'		=> html_entity_decode($modest['description'])
				);
			}
		}



		
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('designer/fashion', $data));
	}

	public function info(){




		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => "Home",
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => "Fashion",
			'href' => $this->url->link('designer/fashion', '', true)
		);

		if(isset($this->request->get['popular_id'])){
			$popular_id=$this->request->get['popular_id'];
		}else{
			$popular_id=0;
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->get['popular_id'])){
			$dbname='popular';
			$dbid=$this->request->get['popular_id'];
		}elseif (isset($this->request->get['trending_id'])) {
			$dbname='trending';
			$dbid=$this->request->get['trending_id'];
		}elseif (isset($this->request->get['modest_id'])) {
			$dbname='modest';
			$dbid=$this->request->get['modest_id'];
		}else{
			$dbid=0;
		}


		$popularQuery=$this->db->query("SELECT * FROM ".DB_PREFIX.$dbname." WHERE ".$dbname."_id='".$dbid."'");
			$data['name']="";
			$data['image']="no-image.png";
			$data['description']="No description found";
		if($popularQuery->num_rows){
			$this->document->setTitle($popularQuery->row['name']." | Fashion");
			$data['name']=$popularQuery->row['name'];
			$data['image']=$popularQuery->row['image'];
			$data['description']=html_entity_decode($popularQuery->row['description'],ENT_QUOTES,'UTF-8');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('designer/fashion_info', $data));

	}
}
