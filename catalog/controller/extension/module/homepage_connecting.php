<?php
class ControllerExtensionModuleHomepageConnecting extends Controller {
	public function index() {
		$this->load->language('extension/module/homepage_connecting');

		

if(null!==$this->config->get('module_homepage_connecting_status')){
	$data['module_homepage_connecting_status'] = $this->config->get('module_homepage_connecting_status');
}else{
	$data['module_homepage_connecting_status']=false;
}

if(null!==$this->config->get('module_homepage_connecting_title')){
	$data['module_homepage_connecting_title'] = $this->config->get('module_homepage_connecting_title');
}else{
	$data['module_homepage_connecting_title']='';
}

if(null!==$this->config->get('module_homepage_connecting_description')){
	$data['module_homepage_connecting_description'] = html_entity_decode($this->config->get('module_homepage_connecting_description'));
}else{
	$data['module_homepage_connecting_description']="";
}

if(null!==$this->config->get('module_homepage_connecting_image')){
	$data['module_homepage_connecting_image'] = HTTP_SERVER.'image/'.$this->config->get('module_homepage_connecting_image');
}else{
	$data['module_homepage_connecting_image']='';
}


if(null!==$this->config->get('module_homepage_connecting_link')){
	$data['module_homepage_connecting_link'] = $this->config->get('module_homepage_connecting_link');
}else{
	$data['module_homepage_connecting_link']='#';
}


		
		return $this->load->view('extension/module/homepage_connecting', $data);
	}
}