<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/clients/ramzey/ramzey_live/admin/');
define('HTTP_CATALOG', 'http://localhost/clients/ramzey/ramzey_live/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/clients/ramzey/ramzey_live/admin/');
define('HTTPS_CATALOG', 'http://localhost/clients/ramzey/ramzey_live/');

// DIR
define('DIR_APPLICATION', 'D:/Ampps/www/clients/ramzey/ramzey_live/admin/');
define('DIR_SYSTEM', 'D:/Ampps/www/clients/ramzey/ramzey_live/system/');
define('DIR_IMAGE', 'D:/Ampps/www/clients/ramzey/ramzey_live/image/');
define('DIR_STORAGE', DIR_SYSTEM . 'storage/');
define('DIR_CATALOG', 'D:/Ampps/www/clients/ramzey/ramzey_live/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'mysql');
define('DB_DATABASE', 'ramzey_live');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
