<?php
// Heading
$_['heading_title']                = 'ESZFaq';

// Text
$_['text_eszfaq']                  = 'ESZFaq';
$_['text_edit']                    = 'Edit ESZFaq';
$_['text_extension']               = 'ESZExtension';
$_['text_success']                 = 'Success: You have modified ESZFaq!';
$_['text_list']                    = 'Faq List';
$_['text_add']                     = 'Add New Faq';

// Entry
$_['entry_status']                 = 'Status';
$_['entry_question']               = 'Question';
$_['entry_answer']                 = 'Answer';
$_['entry_status']                 = 'Status';
$_['entry_sort_order']             = 'Sort Order';

// Column
$_['column_question']              = 'Question';
$_['column_status']                = 'Status';
$_['column_sort_order']            = 'Sort Order';
$_['column_action']                = 'Action';

// Error
$_['error_question']               = 'Question must be greater than 3 and less than 255 characters!';
$_['error_answer']                 = 'Answer must be greater than 3 characters!';
$_['error_permission']             = 'Warning: You do not have permission to modify ESZFaq!';
