<?php
// Heading
$_['heading_title']    = 'Barnoz-Testimonials';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Barnoz-Testimonials Module module!';
$_['text_edit']        = 'Edit Barnoz-Testimonials Module';

// Entry
$_['entry_status']     = 'Status';
$_['entry_title']     = 'Author Name';
$_['entry_description']= 'Testimonial Description';
$_['entry_image']= 'Image ';
$_['entry_button_link']= 'Date';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Barnoz-Testimonials Module module!';