<?php
// Heading
$_['heading_title']     = 'Designers';

// Text
$_['text_success']      = 'Success: You have modified Designers!';
$_['text_list']         = 'Designer List';
$_['text_add']          = 'Add Designer';
$_['text_edit']         = 'Edit Designer';
$_['text_default']      = 'Default';
$_['text_percent']      = 'Percentage';
$_['text_amount']       = 'Fixed Amount';
$_['text_keyword']      = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Column
$_['column_name']       = 'Designer Name';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Designer Name';
$_['entry_store']       = 'Stores';
$_['entry_keyword']     = 'Keyword';
$_['entry_image']       = 'Image';
$_['entry_sort_order']  = 'Sort Order';
$_['entry_type']        = 'Type';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify Designers!';
$_['error_name']        = 'Designer Name must be between 1 and 64 characters!';
$_['error_keyword']     = 'SEO URL already in use!';
$_['error_unique']      = 'SEO URL must be unique!';
$_['error_product']     = 'Warning: This Designer cannot be deleted as it is currently assigned to %s products!';