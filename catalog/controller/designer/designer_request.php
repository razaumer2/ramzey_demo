<?php
class ControllerDesignerDesignerRequest extends Controller {
	public function index() {
		if(($this->request->server['REQUEST_METHOD'] == 'POST') ){


			if(isset($this->request->post['designer_name'])){
				$designer_name=$this->request->post['designer_name'];
			}else{
				$designer_name="";
			}



			if(isset($this->request->post['brand_name'])){
				$brand_name=$this->request->post['brand_name'];
			}else{
				$brand_name="";
			}


			if(isset($this->request->post['email'])){
				$email=$this->request->post['email'];
			}else{
				$email="";
			}


			if(isset($this->request->post['address'])){
				$address=$this->request->post['address'];
			}else{
				$address="";
			}


			if(isset($this->request->post['country'])){
				$country=$this->request->post['country'];
			}else{
				$country="";
			}


			if(isset($this->request->post['zone_id'])){
				$zone_id=$this->request->post['zone_id'];
			}else{
				$zone_id="";
			}


			if(isset($this->request->post['phone'])){
				$phone=$this->request->post['phone'];
			}else{
				$phone="";
			}

			if(isset($this->request->post['years_active'])){
				$years_active=$this->request->post['years_active'];
			}else{
				$years_active="";
			}

			if(isset($this->request->post['incentive'])){
				$incentive=$this->request->post['incentive'];
			}else{
				$incentive="";
			}


	
			
		if(isset($_FILES["pro-image"])){
			$target_dir = DIR_IMAGE."catalog/";
			$target_file = $target_dir . basename($_FILES["pro-image"]["name"]);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			// Check if image file is a actual image or fake image
			 if (move_uploaded_file($_FILES["pro-image"]["tmp_name"], $target_file)) {
			      $brand_logo=  basename( $_FILES["pro-image"]["name"]);
			    }else{
			    	$brand_logo="";
			    }
			}else{
				$brand_logo="";
			}

			$this->db->query("INSERT INTO ".DB_PREFIX."members SET designer_name='".$this->db->escape($designer_name)."',brand_name='".$this->db->escape($brand_name)."',email='".$this->db->escape($email)."',address='".$this->db->escape($address)."',phone='".$this->db->escape($phone)."',incentive='".$this->db->escape($incentive)."',city='".$this->db->escape($zone_id)."',country_id='".$this->db->escape($country)."',years_active='".$this->db->escape($years_active)."',brand_logo='".$this->db->escape($brand_logo)."',status='0',date_added=NOW() ");

			$member_id=$this->db->getLastId();
		if(isset($_FILES["pro-image-brand"])){
			$target_dir_brand = DIR_IMAGE."catalog/";
			$extension=array("jpeg","jpg","png","gif");
			foreach($_FILES["pro-image-brand"]["tmp_name"] as $key=>$tmp_name) {
			    $file_name=$_FILES["pro-image-brand"]["name"][$key];
			    $file_tmp=$_FILES["pro-image-brand"]["tmp_name"][$key];
			    $ext=pathinfo($file_name,PATHINFO_EXTENSION);

			   
			        if(!file_exists($target_dir_brand.$file_name)) {
			            move_uploaded_file($file_tmp=$_FILES["pro-image-brand"]["tmp_name"][$key],$target_dir_brand.$file_name);
			            $this->db->query("INSERT INTO ".DB_PREFIX."designer_images SET member_id='".$member_id."', image='".$file_name."' ");
			          //  echo $file_name."</br>";
			        }
			        else {
			            $filename=basename($file_name,$ext);
			            $newFileName=$filename.time().".".$ext;
			            move_uploaded_file($file_tmp=$_FILES["pro-image-brand"]["tmp_name"][$key],$target_dir_brand.$newFileName);
			            $this->db->query("INSERT INTO ".DB_PREFIX."designer_images SET member_id='".$member_id."', image='".$newFileName."' ");
			           // echo $newFileName."</br>";

			        }
			    
			}
		}
			
					$this->response->redirect($this->url->link('designer/designer_request','&success=1',true));


		}

		if(isset($this->request->get['success'])&&$this->request->get['success']==1){
			$data['success']	=	"Request successfully sent !";
		}else{
			$data['success']	=	false;
		}

		$this->document->setTitle("Request Membership");

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => "Home",
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => "Request Membership",
			'href' => $this->url->link('designer/designer_request', '', true)
		);

		/*if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		} 
		*/
		$data['action']=$this->url->link('designer/designer_request', '', true);

		$this->load->model('localisation/country');

		$data['countries'] = $this->model_localisation_country->getCountries();


		
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('designer/designer_request', $data));
	}

	public function checkDesignerEmail(){
		if(isset($this->request->get['email'])){
			$query=$this->db->query("SELECT member_id FROM ".DB_PREFIX."members WHERE email like '%".$this->request->get['email']."%' ");
			if($query->num_rows){
				echo "1";
			}else{
				echo "0";
			}
		}
	}

	public function checkBrandName(){
		if(isset($this->request->get['brand_name'])){
			$query=$this->db->query("SELECT manufacturer_id from ".DB_PREFIX."manufacturer WHERE name like '".$this->db->escape($this->request->get['brand_name'])."' ");
				if($query->num_rows){
					echo "<p style='color:red !important; font-size:16px;'>Brand Name already exist please choose another one<input type='hidden' id='brand_exist' value='0'></p>";
				}else{
					echo "<p style='color:green !important; font-size:16px;'>Brand Name available<input type='hidden' id='brand_exist' value='1'></p>";
				}
		}
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id'])
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
