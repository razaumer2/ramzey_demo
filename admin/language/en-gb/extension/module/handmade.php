<?php
// Heading
$_['heading_title']    = 'Barnoz-Handmade Revival Module';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Barnoz-Handmade Module module!';
$_['text_edit']        = 'Edit Barnoz-Handmade Module';

// Entry
$_['entry_status']     = 'Status';
$_['entry_title']     = 'Name/Title of Article';
$_['entry_title_1']     = 'Title';
$_['entry_description']= 'Description';
$_['entry_image']= 'Image ';
$_['entry_banner_image']= 'Banner Image';
$_['entry_button_link']= 'Button Link ';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Barnoz-Handmade Module module!';