<?php
class ControllerExtensionModuleFashion extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/fashion');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_fashion', $this->request->post);


			if(isset($this->request->post['trending'])){
				$this->db->query("DELETE FROM ".DB_PREFIX."trending WHERE 1 ");
				foreach ($this->request->post['trending'] as $key => $value) {
					$this->db->query("INSERT INTO ".DB_PREFIX."trending SET name='".$this->db->escape($value['name'])."',image='".$this->db->escape($value['image'])."',description='".$this->db->escape($value['description'])."' ");
				}
			}
			if(isset($this->request->post['popular'])){
				$this->db->query("DELETE FROM ".DB_PREFIX."popular WHERE 1 ");
				foreach ($this->request->post['popular'] as $key => $value) {
					$this->db->query("INSERT INTO ".DB_PREFIX."popular SET name='".$this->db->escape($value['name'])."',image='".$this->db->escape($value['image'])."',description='".$this->db->escape($value['description'])."' ");
				}
			}
			if(isset($this->request->post['modest'])){
					$this->db->query("DELETE FROM ".DB_PREFIX."modest WHERE 1 ");
				foreach ($this->request->post['modest'] as $key => $value) {
					$this->db->query("INSERT INTO ".DB_PREFIX."modest SET name='".$this->db->escape($value['name'])."',image='".$this->db->escape($value['image'])."',description='".$this->db->escape($value['description'])."' ");
				}
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/fashion', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/fashion', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['module_fashion_status'])) {
			$data['module_fashion_status'] = $this->request->post['module_fashion_status'];
		} else {
			$data['module_fashion_status'] = $this->config->get('module_fashion_status');
		}

		if (isset($this->request->post['module_fashion_modest_title'])) {
			$data['module_fashion_modest_title'] = $this->request->post['module_fashion_modest_title'];
		} else {
			$data['module_fashion_modest_title'] = $this->config->get('module_fashion_modest_title');
		}

		if (isset($this->request->post['module_fashion_wedding_title'])) {
			$data['module_fashion_wedding_title'] = $this->request->post['module_fashion_wedding_title'];
		} else {
			$data['module_fashion_wedding_title'] = $this->config->get('module_fashion_wedding_title');
		}

		if (isset($this->request->post['module_fashion_title_2'])) {
			$data['module_fashion_title_2'] = $this->request->post['module_fashion_title_2'];
		} else {
			$data['module_fashion_title_2'] = $this->config->get('module_fashion_title_2');
		}
		

		if (isset($this->request->post['module_wedding_description'])) {
			$data['module_wedding_description'] = $this->request->post['module_wedding_description'];
		} else {
			$data['module_wedding_description'] = $this->config->get('module_wedding_description');
		}

		

		if (isset($this->request->post['module_fashion_description'])) {
			$data['module_fashion_description'] = $this->request->post['module_fashion_description'];
		} else {
			$data['module_fashion_description'] = $this->config->get('module_fashion_description');
		}

		if (isset($this->request->post['module_fashion_description_1'])) {
			$data['module_fashion_description_1'] = $this->request->post['module_fashion_description_1'];
		} else {
			$data['module_fashion_description_1'] = $this->config->get('module_fashion_description_1');
		}

		if (isset($this->request->post['module_fashion_description_2'])) {
			$data['module_fashion_description_2'] = $this->request->post['module_fashion_description_2'];
		} else {
			$data['module_fashion_description_2'] = $this->config->get('module_fashion_description_2');
		}

		if (isset($this->request->post['module_fashion_description_3'])) {
			$data['module_fashion_description_3'] = $this->request->post['module_fashion_description_3'];
		} else {
			$data['module_fashion_description_3'] = $this->config->get('module_fashion_description_3');
		}

		if (isset($this->request->post['module_fashion_description_4'])) {
			$data['module_fashion_description_4'] = $this->request->post['module_fashion_description_4'];
		} else {
			$data['module_fashion_description_4'] = $this->config->get('module_fashion_description_4');
		}

if (isset($this->request->post['module_fashion_wedding_description'])) {
			$data['module_fashion_wedding_description'] = $this->request->post['module_fashion_wedding_description'];
		} else {
			$data['module_fashion_wedding_description'] = $this->config->get('module_fashion_wedding_description');
		}

		

		if (isset($this->request->post['module_fashion_image'])) {
			$data['module_fashion_image'] = $this->request->post['module_fashion_image'];
		} else {
			$data['module_fashion_image'] = $this->config->get('module_fashion_image');
		}

		if (isset($this->request->post['module_fashion_wdding_image'])) {
			$data['module_fashion_wdding_image'] = $this->request->post['module_fashion_wdding_image'];
		} else {
			$data['module_fashion_wdding_image'] = $this->config->get('module_fashion_wdding_image');
		}

if (isset($this->request->post['module_fashion_link'])) {
			$data['module_fashion_link'] = $this->request->post['module_fashion_link'];
		} else {
			$data['module_fashion_link'] = $this->config->get('module_fashion_link');
		}

		if(null!==$this->config->get('module_fashion_wdding_image')){
			$data['thumb_wed']=HTTP_CATALOG.'image/'.$this->config->get('module_fashion_wdding_image');
		}else{
			$data['thumb_wed']=HTTP_CATALOG.'image/no_image.png';
		}


		if(null!==$this->config->get('module_fashion_image')){
			$data['thumb']=HTTP_CATALOG.'image/'.$this->config->get('module_fashion_image');
		}else{
			$data['thumb']=HTTP_CATALOG.'image/no_image.png';
		}

		$data['trendings']=array();
		$data['populars']=array();
		$data['modests']=array();
		$this->load->model('tool/image');
		$trendingQuery=$this->db->query("SELECT * FROM ".DB_PREFIX."trending WHERE 1");

		if($trendingQuery->num_rows){
			foreach ($trendingQuery->rows as $key => $trending) {
				$data['trendings'][]=array(
						'trending_id'		=> $trending['trending_id'],
						'name'				=> $trending['name'],
						'image'				=> $trending['image'],
						'thumb_trend'		=> $this->model_tool_image->resize($trending['image'],100,100),
						'description'		=> html_entity_decode($trending['description'])
				);
			}
			
		}

		$popularQuery=$this->db->query("SELECT * FROM ".DB_PREFIX."popular WHERE 1");

		if($popularQuery->num_rows){
			//$data['trendings']=$popularQuery->rows;
			foreach ($popularQuery->rows as $key => $popular) {
				$data['populars'][]=array(
						'popular_id'		=> $popular['popular_id'],
						'name'				=> $popular['name'],
						'image'				=> $popular['image'],
						'thumb_pop'			=> $this->model_tool_image->resize($popular['image'],100,100),
						'description'		=> html_entity_decode($popular['description'])
				);
			}
		}

		$modestQuery=$this->db->query("SELECT * FROM ".DB_PREFIX."modest WHERE 1");

		if($modestQuery->num_rows){
			//$data['trendings']=$modestQuery->rows;
			foreach ($modestQuery->rows as $key => $modest) {
				$data['modests'][]=array(
						'modest_id'			=> $modest['modest_id'],
						'name'				=> $modest['name'],
						'image'				=> $modest['image'],
						'thumb_mod'			=> $this->model_tool_image->resize($modest['image'],100,100),
						'description'		=> html_entity_decode($modest['description'])
				);
			}
		}

		//$data['thumb']=HTTP_CATALOG.'image/'.$this->config->get('module_fashion_image');

		$data['placeholder']=HTTP_CATALOG.'image/no_image.png';

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/fashion', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/fashion')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}