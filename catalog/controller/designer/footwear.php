<?php
class ControllerDesignerFootwear extends Controller {
	public function index() {

		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => "Home",
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => "Footwear",
			'href' => $this->url->link('designer/footwear', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		} 
		$this->load->model('tool/image');

		
		
		$this->document->setTitle("Footwear");

		if(null!==$this->config->get('module_footwarepage_title')){
			$data['module_footwarepage_title']	=	$this->config->get('module_footwarepage_title');
		}else{
			$data['module_footwarepage_title']	=	"";
		}

		if(null!==$this->config->get('module_footwarepage_description')){
			$data['module_footwarepage_description']	=	html_entity_decode($this->config->get('module_footwarepage_description'),ENT_QUOTES);
		}else{
			$data['module_footwarepage_description']	=	"";
		}

		if(null!==$this->config->get('module_footwarepage_image')){
			$data['main_image']	=	HTTPS_SERVER.'image/'.$this->config->get('module_footwarepage_image');
		}else{
			$data['main_image']	=	HTTPS_SERVER.'image/no-image.png';
		}

		if(null!==$this->config->get('module_footwarepage_title_1')){
			$data['module_footwarepage_title_1']	=	$this->config->get('module_footwarepage_title_1');
		}else{
			$data['module_footwarepage_title_1']	=	"";
		}

		if(null!==$this->config->get('module_footwarepage_description_1')){
			$data['module_footwarepage_description_1']	=	html_entity_decode($this->config->get('module_footwarepage_description_1'),ENT_QUOTES);
		}else{
			$data['module_footwarepage_description_1']	=	"";
		}

		if(null!==$this->config->get('module_footwarepage_image_1')){
			$data['traditional_image']	=	HTTPS_SERVER.'image/'.$this->config->get('module_footwarepage_image_1');
		}else{
			$data['traditional_image']	=	HTTPS_SERVER.'image/no-image.png';
		}

		if(null!==$this->config->get('module_footwarepage_title_2')){
			$data['module_footwarepage_title_2']	=	$this->config->get('module_footwarepage_title_2');
		}else{
			$data['module_footwarepage_title_2']	=	"";
		}

		if(null!==$this->config->get('module_footwarepage_description_2')){
			$data['module_footwarepage_description_2']	=	html_entity_decode($this->config->get('module_footwarepage_description_2'),ENT_QUOTES);
		}else{
			$data['module_footwarepage_description_2']	=	"";
		}

		if(null!==$this->config->get('module_footwarepage_image_2')){
			$data['blend_image']	=	HTTPS_SERVER.'image/'.$this->config->get('module_footwarepage_image_2');
		}else{
			$data['blend_image']	=	HTTPS_SERVER.'image/no-image.png';
		}

		if (null!==$this->config->get('module_footwarepage_products')) {
			$products = $this->config->get('module_footwarepage_products');
		} else {
			$products = array();
		}


		$this->load->model('catalog/product');

		$this->load->model('tool/image');

$data['products']=array();
			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], 280, 200);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', 280, 200);
					}

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}

					$data['products'][] = array(
						'product_id'  => $product_info['product_id'],
						'designer_name' => $this->getDesignerName($product_info['manufacturer_id']),
						'thumb'       => $image,
						'name'        => $product_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
				}
			}
		

		
		
		$data['action']=$this->url->link('designer/footwear', '', true);



		
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('designer/footware', $data));
	}

	public function getDesignerName($manufacturer_id){
		$query=	$this->db->query("SELECT name from ".DB_PREFIX."manufacturer WHERE manufacturer_id='".$manufacturer_id."' ");
		if($query->num_rows){
			return $query->row['name'];
		}else{
			return "";
		}
	}

	
}
