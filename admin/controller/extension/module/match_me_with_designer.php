<?php
class ControllerExtensionModuleMatchMeWithDesigner extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/match_me_with_designer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_match_me_with_designer', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/match_me_with_designer', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/match_me_with_designer', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		$data['user_token']=$this->session->data['user_token'];

		if (isset($this->request->post['module_match_me_with_designer_status'])) {
			$data['module_match_me_with_designer_status'] = $this->request->post['module_match_me_with_designer_status'];
		} else {
			$data['module_match_me_with_designer_status'] = $this->config->get('module_match_me_with_designer_status');
		}

		$data['custom_design_orders']=array();

	$queryData=	$this->db->query("SELECT * FROM ".DB_PREFIX."custom_designs WHERE 1 ");
	if($queryData->num_rows){
		$data['custom_design_orders']=$queryData->rows;
	}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/match_me_with_designer', $data));
	}

	public function getMeasurementValues(){
		if(isset($this->request->get['custom_design_id'])){
$queryData=	$this->db->query("SELECT * FROM ".DB_PREFIX."custom_designs WHERE custom_design_id='".$this->request->get['custom_design_id']."' ");

echo "<tr>

<td>".$queryData->row['fabric']."</td>
<td>".$queryData->row['neckline_measurement']."</td>
<td>".$queryData->row['bust_line']."</td>
<td>".$queryData->row['waist_line']."</td>
<td>".$queryData->row['hip_line']."</td>
<td>".$queryData->row['sleeve_length']."</td>
<td>".$queryData->row['cuff_size']."</td>
<td>".$queryData->row['shoulder_line']."</td>
<td>".$queryData->row['arm_hole']."</td>
<td>".$queryData->row['bottom_width']."</td>
<td>".$queryData->row['arm_circumference']."</td>
<td>".$queryData->row['caftan_length']."</td>
<td>".$queryData->row['neck_deep']."</td>
<td><img src='../image/catalog/".$queryData->row['sample_image']."'</td>
</tr>";
		}
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/match_me_with_designer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}