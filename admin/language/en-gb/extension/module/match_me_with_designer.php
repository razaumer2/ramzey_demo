<?php
// Heading
$_['heading_title']    = 'Barnoz-Match Me With Designer';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Match Me With Designer module!';
$_['text_edit']        = 'Edit Match Me With Designer Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Match Me With Designer module!';