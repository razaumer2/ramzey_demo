<?php
class ControllerInformationMadetoorder extends Controller {
	public function index() {
		$this->load->language('information/madetoorder');

		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		

		
			$this->document->setTitle("Made To Order");
			$this->document->setDescription("Made To Order");
			$this->document->setKeywords("Made To Order");

			$data['breadcrumbs'][] = array(
				'text' => "Made to order",
				'href' => $this->url->link('information/madetoorder', '',true)
			);

			$data['testimonials']=array();

			$getTestimonialsData=$this->db->query("SELECT * FROM ".DB_PREFIX."testimonials WHERE 1  ");
		if($getTestimonialsData->num_rows){
			foreach ($getTestimonialsData->rows as $key => $value) {
				$data['testimonials'][]=array(
					'testimonial_id'		=>$value['testimonial_id'],
					'author'				=>$value['author'],
					'description'				=>html_entity_decode($value['description']),
					'status'				=>$value['status'],
					'date_added'				=>$value['date_added']

				);
			}
		}

			$data['heading_title'] = "Made to Order";
			$data['getMadeToOrderGallery']=array();
$data['getMadeToOrderGallery']=$this->db->query("SELECT * FROM ".DB_PREFIX."made_to_order_list WHERE 1 ")->rows;
//			var_dump($data['getMadeToOrderGallery']);
			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('information/madetoorder', $data));
		
	}

	public function agree() {
		$this->load->model('catalog/information');

		if (isset($this->request->get['information_id'])) {
			$information_id = (int)$this->request->get['information_id'];
		} else {
			$information_id = 0;
		}

		$output = '';

		$information_info = $this->model_catalog_information->getInformation($information_id);

		if ($information_info) {
			$output .= html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8') . "\n";
		}

		$this->response->setOutput($output);
	}
}