<?php
class ControllerExtensionModuleFootware extends Controller {
	public function index() {
		$this->load->language('extension/module/footware');

		

if(null!==$this->config->get('module_footware_status')){
	$data['module_footware_status'] = $this->config->get('module_footware_status');
}else{
	$data['module_footware_status']=false;
}

if(null!==$this->config->get('module_footware_title')){
	$data['module_footware_title'] = $this->config->get('module_footware_title');
}else{
	$data['module_footware_title']='';
}

if(null!==$this->config->get('module_footware_description')){
	$data['module_footware_description'] = html_entity_decode($this->config->get('module_footware_description'));
}else{
	$data['module_footware_description']="";
}

if(null!==$this->config->get('module_footware_image')){
	$data['module_footware_image'] = HTTP_SERVER.'image/'.$this->config->get('module_footware_image');
}else{
	$data['module_footware_image']='';
}


if(null!==$this->config->get('module_footware_link')){
	$data['module_footware_link'] = $this->config->get('module_footware_link');
}else{
	$data['module_footware_link']='#';
}


		
		return $this->load->view('extension/module/footware', $data);
	}
}