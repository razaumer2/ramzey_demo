<?php
class ControllerExtensionModuleHandmade extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/handmade');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_handmade', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module/handmade', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/handmade', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/handmade', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['module_handmade_status'])) {
			$data['module_handmade_status'] = $this->request->post['module_handmade_status'];
		} else {
			$data['module_handmade_status'] = $this->config->get('module_handmade_status');
		}

		if (isset($this->request->post['module_handmade_title'])) {
			$data['module_handmade_title'] = $this->request->post['module_handmade_title'];
		} else {
			$data['module_handmade_title'] = $this->config->get('module_handmade_title');
		}

		if (isset($this->request->post['module_handmade_title1'])) {
			$data['module_handmade_title1'] = $this->request->post['module_handmade_title1'];
		} else {
			$data['module_handmade_title1'] = $this->config->get('module_handmade_title1');
		}

		if (isset($this->request->post['module_handmade_title2'])) {
			$data['module_handmade_title2'] = $this->request->post['module_handmade_title2'];
		} else {
			$data['module_handmade_title2'] = $this->config->get('module_handmade_title2');
		}
		if (isset($this->request->post['module_handmade_title4'])) {
			$data['module_handmade_title4'] = $this->request->post['module_handmade_title4'];
		} else {
			$data['module_handmade_title4'] = $this->config->get('module_handmade_title4');
		}
		if (isset($this->request->post['module_handmade_emb_title'])) {
			$data['module_handmade_emb_title'] = $this->request->post['module_handmade_emb_title'];
		} else {
			$data['module_handmade_emb_title'] = $this->config->get('module_handmade_emb_title');
		}
		if (isset($this->request->post['module_handmade_foot_title'])) {
			$data['module_handmade_foot_title'] = $this->request->post['module_handmade_foot_title'];
		} else {
			$data['module_handmade_foot_title'] = $this->config->get('module_handmade_foot_title');
		}

		

		if (isset($this->request->post['module_handmade_description'])) {
			$data['module_handmade_description'] = $this->request->post['module_handmade_description'];
		} else {
			$data['module_handmade_description'] = $this->config->get('module_handmade_description');
		}

		if (isset($this->request->post['module_handmade_description1'])) {
			$data['module_handmade_description1'] = $this->request->post['module_handmade_description1'];
		} else {
			$data['module_handmade_description1'] = $this->config->get('module_handmade_description1');
		}

		if (isset($this->request->post['module_handmade_description2'])) {
			$data['module_handmade_description2'] = $this->request->post['module_handmade_description2'];
		} else {
			$data['module_handmade_description2'] = $this->config->get('module_handmade_description2');
		}

		if (isset($this->request->post['module_handmade_description3'])) {
			$data['module_handmade_description3'] = $this->request->post['module_handmade_description3'];
		} else {
			$data['module_handmade_description3'] = $this->config->get('module_handmade_description3');
		}
		if (isset($this->request->post['module_handmade_description4'])) {
			$data['module_handmade_description4'] = $this->request->post['module_handmade_description4'];
		} else {
			$data['module_handmade_description4'] = $this->config->get('module_handmade_description4');
		}
		if (isset($this->request->post['module_handmade_description5'])) {
			$data['module_handmade_description5'] = $this->request->post['module_handmade_description5'];
		} else {
			$data['module_handmade_description5'] = $this->config->get('module_handmade_description5');
		}
		if (isset($this->request->post['module_handmade_description6'])) {
			$data['module_handmade_description6'] = $this->request->post['module_handmade_description6'];
		} else {
			$data['module_handmade_description6'] = $this->config->get('module_handmade_description6');
		}

		if (isset($this->request->post['module_handmade_description7'])) {
			$data['module_handmade_description7'] = $this->request->post['module_handmade_description7'];
		} else {
			$data['module_handmade_description7'] = $this->config->get('module_handmade_description7');
		}
		if (isset($this->request->post['module_handmade_description8'])) {
			$data['module_handmade_description8'] = $this->request->post['module_handmade_description8'];
		} else {
			$data['module_handmade_description8'] = $this->config->get('module_handmade_description8');
		}
		if (isset($this->request->post['module_handmade_description9'])) {
			$data['module_handmade_description9'] = $this->request->post['module_handmade_description9'];
		} else {
			$data['module_handmade_description9'] = $this->config->get('module_handmade_description9');
		}


		if (isset($this->request->post['module_handmade_banner_image'])) {
			$data['module_handmade_banner_image'] = $this->request->post['module_handmade_banner_image'];
		} else {
			$data['module_handmade_banner_image'] = $this->config->get('module_handmade_banner_image');
		}

		
		if(null!==$this->config->get('module_handmade_banner_image')){
			$data['banner_thumb']=HTTP_CATALOG.'image/'.$this->config->get('module_handmade_banner_image');
		}else{
			$data['banner_thumb']=HTTP_CATALOG.'image/placeholder.png';
		}


		if (isset($this->request->post['module_handmade_image1'])) {
			$data['module_handmade_image1'] = $this->request->post['module_handmade_image1'];
		} else {
			$data['module_handmade_image1'] = $this->config->get('module_handmade_image1');
		}

		
		if(null!==$this->config->get('module_handmade_image1')){
			$data['thumb_1']=HTTP_CATALOG.'image/'.$this->config->get('module_handmade_image1');
		}else{
			$data['thumb_1']=HTTP_CATALOG.'image/placeholder.png';
		}

		if (isset($this->request->post['module_handmade_image2'])) {
			$data['module_handmade_image2'] = $this->request->post['module_handmade_image2'];
		} else {
			$data['module_handmade_image2'] = $this->config->get('module_handmade_image2');
		}

		
		if(null!==$this->config->get('module_handmade_image2')){
			$data['thumb_2']=HTTP_CATALOG.'image/'.$this->config->get('module_handmade_image2');
		}else{
			$data['thumb_2']=HTTP_CATALOG.'image/placeholder.png';
		}
		if (isset($this->request->post['module_handmade_image3'])) {
			$data['module_handmade_image3'] = $this->request->post['module_handmade_image3'];
		} else {
			$data['module_handmade_image3'] = $this->config->get('module_handmade_image3');
		}

		
		if(null!==$this->config->get('module_handmade_image3')){
			$data['thumb_3']=HTTP_CATALOG.'image/'.$this->config->get('module_handmade_image3');
		}else{
			$data['thumb_3']=HTTP_CATALOG.'image/placeholder.png';
		}
		if (isset($this->request->post['module_handmade_image4'])) {
			$data['module_handmade_image4'] = $this->request->post['module_handmade_image4'];
		} else {
			$data['module_handmade_image4'] = $this->config->get('module_handmade_image4');
		}

		
		if(null!==$this->config->get('module_handmade_image4')){
			$data['thumb_4']=HTTP_CATALOG.'image/'.$this->config->get('module_handmade_image4');
		}else{
			$data['thumb_4']=HTTP_CATALOG.'image/placeholder.png';
		}
		if (isset($this->request->post['module_handmade_image5'])) {
			$data['module_handmade_image5'] = $this->request->post['module_handmade_image5'];
		} else {
			$data['module_handmade_image5'] = $this->config->get('module_handmade_image5');
		}

		
		if(null!==$this->config->get('module_handmade_image5')){
			$data['thumb_5']=HTTP_CATALOG.'image/'.$this->config->get('module_handmade_image5');
		}else{
			$data['thumb_5']=HTTP_CATALOG.'image/placeholder.png';
		}
		if (isset($this->request->post['module_handmade_image6'])) {
			$data['module_handmade_image6'] = $this->request->post['module_handmade_image6'];
		} else {
			$data['module_handmade_image6'] = $this->config->get('module_handmade_image6');
		}

		
		if(null!==$this->config->get('module_handmade_image6')){
			$data['thumb_6']=HTTP_CATALOG.'image/'.$this->config->get('module_handmade_image6');
		}else{
			$data['thumb_6']=HTTP_CATALOG.'image/placeholder.png';
		}
		if (isset($this->request->post['module_handmade_image7'])) {
			$data['module_handmade_image7'] = $this->request->post['module_handmade_image7'];
		} else {
			$data['module_handmade_image7'] = $this->config->get('module_handmade_image7');
		}

		
		if(null!==$this->config->get('module_handmade_image7')){
			$data['thumb_7']=HTTP_CATALOG.'image/'.$this->config->get('module_handmade_image7');
		}else{
			$data['thumb_7']=HTTP_CATALOG.'image/placeholder.png';
		}
		if (isset($this->request->post['module_handmade_image8'])) {
			$data['module_handmade_image8'] = $this->request->post['module_handmade_image8'];
		} else {
			$data['module_handmade_image8'] = $this->config->get('module_handmade_image8');
		}

		
		if(null!==$this->config->get('module_handmade_image8')){
			$data['thumb_8']=HTTP_CATALOG.'image/'.$this->config->get('module_handmade_image8');
		}else{
			$data['thumb_8']=HTTP_CATALOG.'image/placeholder.png';
		}
		if (isset($this->request->post['module_handmade_image9'])) {
			$data['module_handmade_image9'] = $this->request->post['module_handmade_image9'];
		} else {
			$data['module_handmade_image9'] = $this->config->get('module_handmade_image9');
		}

		
		if(null!==$this->config->get('module_handmade_image9')){
			$data['thumb_9']=HTTP_CATALOG.'image/'.$this->config->get('module_handmade_image9');
		}else{
			$data['thumb_9']=HTTP_CATALOG.'image/placeholder.png';
		}
		if (isset($this->request->post['module_handmade_image10'])) {
			$data['module_handmade_image10'] = $this->request->post['module_handmade_image10'];
		} else {
			$data['module_handmade_image10'] = $this->config->get('module_handmade_image10');
		}

		
		if(null!==$this->config->get('module_handmade_image10')){
			$data['thumb_10']=HTTP_CATALOG.'image/'.$this->config->get('module_handmade_image10');
		}else{
			$data['thumb_10']=HTTP_CATALOG.'image/placeholder.png';
		}
		if (isset($this->request->post['module_handmade_image11'])) {
			$data['module_handmade_image11'] = $this->request->post['module_handmade_image11'];
		} else {
			$data['module_handmade_image11'] = $this->config->get('module_handmade_image11');
		}

		
		if(null!==$this->config->get('module_handmade_image11')){
			$data['thumb_11']=HTTP_CATALOG.'image/'.$this->config->get('module_handmade_image11');
		}else{
			$data['thumb_11']=HTTP_CATALOG.'image/placeholder.png';
		}
		if (isset($this->request->post['module_handmade_image12'])) {
			$data['module_handmade_image12'] = $this->request->post['module_handmade_image12'];
		} else {
			$data['module_handmade_image12'] = $this->config->get('module_handmade_image12');
		}

		
		if(null!==$this->config->get('module_handmade_image12')){
			$data['thumb_12']=HTTP_CATALOG.'image/'.$this->config->get('module_handmade_image12');
		}else{
			$data['thumb_12']=HTTP_CATALOG.'image/placeholder.png';
		}

		$data['placeholder']=HTTP_CATALOG.'image/placeholder.png';

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/handmade', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/handmade')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}