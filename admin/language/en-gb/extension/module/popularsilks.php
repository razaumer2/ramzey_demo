<?php
// Heading
$_['heading_title']    = 'Barnoz-Popularsilks';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Barnoz-Popularsilks module!';
$_['text_edit']        = 'Edit Barnoz-Popularsilks Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Barnoz-Popularsilks module!';